import { Component, OnInit } from "@angular/core"
import { Quiz } from "../models/quiz.model"
import { QuizService } from "../core/Services/quiz.service"
import { ActivatedRoute } from "@angular/router"
import { Question } from "../models/question.model"
import { Answer } from "../models/answer.model"

@Component({
  selector: "quiz",
  templateUrl: "./quiz.component.html",
  styleUrl: "./quiz.component.scss",
})
export class QuizComponent implements OnInit {
  $quiz?: Quiz
  // map <questionId, answerId>
  listAnswerIdSelectedGood: Map<number, number>
  errorMessage: string = ""
  scoreUser?: number
  nbQuestions: number = 0

  constructor(private quizService: QuizService, private _route: ActivatedRoute) {
    this.listAnswerIdSelectedGood = new Map<number, number>()
  }

  ngOnInit() {
    this.quizService.findById(Number(this._route.snapshot.paramMap.get('id'))).subscribe((quiz: Quiz) => {
      this.$quiz = quiz
      this.nbQuestions = quiz.questions.length
    })
  }

  validerQuiz() {
    if (this.verificationAllQuestionAsOneAnswer()) {
      // @ts-ignore
      this.quizService.valideQuiz(Array.from(this.listAnswerIdSelectedGood.values()), this.$quiz.quizId).subscribe((score: number) => {
        this.scoreUser = score
      })
    }
  }

  verificationAllQuestionAsOneAnswer(): boolean {
    if (Array.from(this.listAnswerIdSelectedGood.values()).length < this.nbQuestions) {
      this.errorMessage = "Merci de répondre à toutes les questions"
      return false
    }
    this.errorMessage = ""
    return true
  }

  onAnswerToQuestionSelected(event: { question: Question; answer: Answer }) {
    // @ts-ignore
    this.listAnswerIdSelectedGood.set(event.question.questionId, event.answer.answerId)
  }
}
