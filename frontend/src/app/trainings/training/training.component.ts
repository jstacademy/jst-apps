import { Component, Input } from "@angular/core"
import { Training } from "../../models/training.model"

@Component({
  selector: 'training',
  templateUrl: './training.component.html',
  styleUrl: './training.component.scss'
})
export class TrainingComponent  {
  @Input()
  training! : Training;
}
