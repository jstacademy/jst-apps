import { Component, input } from "@angular/core"
import { Training } from "../../models/training.model"
import { map, Observable } from "rxjs"
import { ActivatedRoute, Router } from "@angular/router"
import { TrainingService } from "../../core/Services/training.service"

@Component({
  selector: 'training-form',
  templateUrl: './training-form.component.html',
  styleUrl: './training-form.component.scss'
})
export class TrainingFormComponent {
  training$: Observable<Training> =  this._route.data.pipe(map((data) => data["training"]))
  id: any = this._route.snapshot.params["id"]

  constructor(
    private _route: ActivatedRoute,
    private trainingsService: TrainingService,
    private router: Router
  ) {}

  save(training: Training) {
    if (this.isNew()) {
      this.trainingsService.create(training).subscribe(() => {
        this.router.navigate(["trainings"])
      })
    } else {
      this.trainingsService.update(training).subscribe(() => {
        this.router.navigate(["trainings"])
      })
    }
  }

  deleteTraining(training: Training) {
    if (this.isNew()) {

    } else {
      this.trainingsService.delete(training).subscribe(() => {
        this.router.navigate(["trainings"])
        })
    }
  }

  isNew(): boolean {
    return this.id == "new"
  }
}
