import { inject } from "@angular/core"
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from "@angular/router"
import { Training } from "models/training.model"
import { TrainingService } from "core/Services/training.service"
import { Observable } from "rxjs"

export const TrainingDetailsResolver: ResolveFn<Training> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
) => {
  if (route.params["id"] == "new") {
    return new Observable((observer) => {
      observer.next({ title: "", description: "", courses: [] })
    })
  }
  return inject(TrainingService).findById(parseInt(route.params["id"], 10))
}
