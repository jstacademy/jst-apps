import { Component } from "@angular/core"
import { map, Observable } from "rxjs"
import { Training } from "../models/training.model"
import { ActivatedRoute } from "@angular/router"

@Component({
  selector: 'trainings',
  templateUrl: './trainings.component.html',
  styleUrl: './trainings.component.scss'
})
export class TrainingsComponent {
  trainings$: Observable<Training[]> = this._route.data.pipe(map((data) => data["trainings"]));

  constructor(private _route: ActivatedRoute) {
  }
}

