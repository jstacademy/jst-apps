import { inject } from "@angular/core"
import { ResolveFn } from "@angular/router"
import { Training } from "../models/training.model"
import { TrainingService } from "../core/Services/training.service"

export const TrainingsResolver: ResolveFn<Training[]> = () => {
  return inject(TrainingService).findAll()
}
