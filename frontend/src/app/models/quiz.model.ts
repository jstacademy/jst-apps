import { Lesson } from "./lesson.model"
import { Question } from "./question.model"

export interface Quiz {
  quizId?: bigint,
  title: string,
  content: string,
  questions : Question[],
  lesson : Lesson}
