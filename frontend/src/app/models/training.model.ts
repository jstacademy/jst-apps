import { Courses } from "./courses.model"

export interface Training {
  trainingId?: bigint,
  title: string,
  description: string
  courses : Courses[]
}
