import { Quiz } from "./quiz.model"
import { Answer } from "./answer.model"

export interface Question {
  questionId?: bigint,
  question: string,
  quiz: Quiz
  answers : Answer[]}
