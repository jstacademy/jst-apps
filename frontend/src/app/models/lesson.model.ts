import { Quiz } from "./quiz.model"
import { Courses } from "./courses.model"

export interface Lesson {
  lessonId?: bigint,
  title: string,
  content: string,
  order_lesson: number,
  quiz: Quiz,
  course: Courses
}
