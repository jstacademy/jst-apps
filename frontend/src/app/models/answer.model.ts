import { Question } from "./question.model"

export interface Answer {
  answerId?: bigint,
  answer: string,
  isTrue: boolean,
  question: Question
}
