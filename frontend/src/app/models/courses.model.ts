import { Lesson } from "./lesson.model"
import { Training } from "./training.model"

export interface Courses {
  coursesId?: bigint,
  title: string,
  order_course: number,
  lessons : Lesson[],
  training: Training
}
