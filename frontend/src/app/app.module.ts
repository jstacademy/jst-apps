import { NgModule } from "@angular/core"
import { BrowserModule } from "@angular/platform-browser"
import { AppRoutingModule } from "app-routing.module"
import { AppComponent } from "app.component"
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"
import { NavbarComponent } from "core/navbar/navbar.component"
import { MatListModule } from "@angular/material/list"
import { HomeComponent } from "home/home.component"
import { FormsModule } from "@angular/forms"
import { MatIconModule } from "@angular/material/icon"
import { MatButtonModule } from "@angular/material/button"
import { HttpClientModule } from "@angular/common/http"
import { TrainingsComponent } from "./trainings/trainings.component"
import { MatCardModule } from "@angular/material/card"
import { MatGridListModule } from "@angular/material/grid-list"
import { CourseComponent } from "./course/course.component"
import { TrainingFormComponent } from "./trainings/training-form/training-form.component"
import { TrainingComponent } from "./trainings/training/training.component"
import { QuizComponent } from "./quiz/quiz.component"
import { AdminComponent } from "./admin/admin.component"
import { TrainingAdminComponent } from "./training-admin/training-admin.component"
import {
  MatCell,
  MatCellDef,
  MatColumnDef,
  MatHeaderCell,
  MatHeaderCellDef, MatHeaderRow,
  MatHeaderRowDef, MatRow, MatRowDef,
  MatTable,
} from "@angular/material/table"
import { QuestionComponent } from "./question/question.component"
import { CdkMenuItemRadio } from "@angular/cdk/menu"
import { MatRadioButton, MatRadioGroup } from "@angular/material/radio"
import { MatOption, MatSelect } from "@angular/material/select"
import { MatFormField, MatInput, MatLabel } from "@angular/material/input"
import {
  MatAccordion,
  MatExpansionPanel,
  MatExpansionPanelDescription,
  MatExpansionPanelTitle,
} from "@angular/material/expansion"
import {
  MatNestedTreeNode,
  MatTree,
  MatTreeNode,
  MatTreeNodeDef,
  MatTreeNodeOutlet,
  MatTreeNodeToggle,
} from "@angular/material/tree"
import {
  MatStep,
  MatStepLabel,
  MatStepper,
  MatStepperIcon,
  MatStepperNext,
  MatStepperPrevious,
} from "@angular/material/stepper"
import { FooterComponent } from "./core/footer/footer.component"
import { NgOptimizedImage } from "@angular/common"

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    TrainingsComponent,
    CourseComponent,
    TrainingFormComponent,
    TrainingComponent,
    QuizComponent,
    TrainingComponent,
    AdminComponent,
    TrainingAdminComponent,
    QuestionComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatListModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    MatCardModule, //Training
    MatButtonModule, // Training
    MatGridListModule,
    CdkMenuItemRadio,
    MatRadioButton,
    MatRadioGroup,
    MatTable,
    MatColumnDef,
    MatHeaderCell,
    MatCell,
    MatCellDef,
    MatHeaderCellDef,
    MatHeaderRowDef,
    MatHeaderRow,
    MatRow,
    MatRowDef,
    MatFormField,
    MatSelect,
    MatOption,
    MatLabel,
    MatInput,
    MatLabel,
    MatFormField,
    MatAccordion,
    MatExpansionPanel,
    MatExpansionPanelTitle,
    MatExpansionPanelDescription,
    MatTree,
    MatTreeNode,
    MatNestedTreeNode,
    MatTreeNodeDef,
    MatTreeNodeToggle,
    MatTreeNodeOutlet,
    MatStepper,
    MatStep,
    MatStepperNext,
    MatStepperPrevious,
    MatStepperIcon,
    MatStepLabel,
    NgOptimizedImage,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
