import { Component, OnInit } from "@angular/core"
import { map, Observable } from "rxjs";
import { Training } from "models/training.model";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  training$: Observable<Training> = this._route.data.pipe(map((data) => data["training"]));

  constructor(private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.training$.subscribe(training => {
      training.courses = training.courses.sort((a, b) => a.order_course - b.order_course);
      training.courses.forEach(course => {
        course.lessons = course.lessons.sort((a, b) => a.order_lesson - b.order_lesson);
      });
      return training;
    });
  }
}
