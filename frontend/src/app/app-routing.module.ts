import { NgModule } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { HomeComponent } from "home/home.component"
import { TrainingsComponent } from "./trainings/trainings.component"
import { TrainingsResolver } from "./trainings/trainings.resolver"
import { TrainingDetailsResolver } from "./trainings/training-form/training.resolver"
import { CourseComponent } from "./course/course.component"
import { TrainingFormComponent } from "./trainings/training-form/training-form.component"
import { PageNotFoundComponent } from "./core/page-not-found/page-not-found.component"
import { QuizComponent } from "./quiz/quiz.component"
import { AdminComponent } from "./admin/admin.component"


const routes: Routes = [
  {
    path: "",
    title: "Accueil",
    component: HomeComponent,
    resolve: {
      trainings: TrainingsResolver,
    }
  },
  {
    path: "admin",
    title: "Administration",
    component: AdminComponent,
    resolve: {
      trainings: TrainingsResolver,
    }
  },
  {
    path: "course/:id",
    title: "course pages",
    component: CourseComponent,
    resolve: {
      training: TrainingDetailsResolver,
    },
  },
  {
    path: "trainings/edit/:id",
    component: TrainingFormComponent,
    resolve: {
      training: TrainingDetailsResolver,
    },
  },
  {
    path: 'trainings',
    title: "Trainings",
    resolve: {
      trainings: TrainingsResolver
    },
    component: TrainingsComponent,
    children: [
    ]
  },
  {
    path: 'quiz/:id',
    title: "Quiz",
    component: QuizComponent
  },
  {
    path: '**',
    title: "Page introuvable",
    component: PageNotFoundComponent,
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
