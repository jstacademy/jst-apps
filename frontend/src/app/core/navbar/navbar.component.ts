import { Component } from "@angular/core"
import { Link } from "models/links.model"

@Component({
  selector: "navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent {
  links: Link[] = []

  constructor() {
    this.links.push({name: "Accueil", href:""})
    this.links.push({ name: "Trainings", href: "trainings" })
    this.links.push({ name: "Admin", href: "admin" })

  }
}
