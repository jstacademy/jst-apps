import { Injectable } from "@angular/core"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { Training } from "../../models/training.model"
import { environment } from "../../../environments/environment"

@Injectable({
  providedIn: "root",
})
export class TrainingService {
  constructor(private http: HttpClient) {}

  private trainingUrl = environment.url + "trainings";

  findAll(): Observable<Training[]> {
    return this.http.get<Training[]>(this.trainingUrl);
  }

  findById(id: number): Observable<Training> {
    return this.http.get<Training>(`${this.trainingUrl}/${id}`);
  }

  create(training: Training): Observable<Training> {
    return this.http.post<Training>(this.trainingUrl, training)
  }

  update(training: Training): Observable<Training> {
    return this.http.put<Training>(`${this.trainingUrl}/${training.trainingId}`, training)
  }

  delete(training: Training) {
    return this.http.delete(`${this.trainingUrl}/${training.trainingId}`)
  }
}
