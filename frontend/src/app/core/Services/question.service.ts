import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { environment } from "../../../environments/environment"
import { Question } from "../../models/question.model"

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  private questionsUrl = environment.url + "question";


  constructor(private http: HttpClient) {}

  delete(question: Question) {
    return this.http.delete(`${this.questionsUrl}/${question.questionId}`)
  }
}
