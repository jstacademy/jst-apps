import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment"
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs"
import { Quiz } from "../../models/quiz.model"

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http: HttpClient) {}

  private quizUrl = environment.url + "quiz";

  findById(quizId: number): Observable<Quiz> {
    return this.http.get<Quiz>(`${this.quizUrl}/${quizId}`)
  }

  create(quiz: Quiz): Observable<Quiz> {
    return this.http.post<Quiz>(this.quizUrl, quiz)
  }

  update(quiz: Quiz): Observable<Quiz> {
    return this.http.put<Quiz>(`${this.quizUrl}/${quiz.quizId}`, quiz)
  }

  delete(quiz: Quiz) {
    return this.http.delete(`${this.quizUrl}/${quiz.quizId}`)
  }

  valideQuiz(listAnswer: number[], quizId: number): Observable<number> {
    return this.http.post<number>(`${this.quizUrl}/checkAnswers/${quizId}`, listAnswer)
  }
}
