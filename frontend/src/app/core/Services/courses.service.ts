import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs"
import { Courses } from "../../models/courses.model"
import { environment } from "../../../environments/environment"
@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient) {}

  private coursesUrl = environment.url + "courses";

  findAll(): Observable<Courses[]> {
    return this.http.get<Courses[]>(this.coursesUrl);
  }

  findById(id: number): Observable<Courses> {
    return this.http.get<Courses>(`${this.coursesUrl}/${id}`);
  }

  create(courses: Courses): Observable<Courses> {
    return this.http.post<Courses>(this.coursesUrl, courses)
  }

  update(courses: Courses): Observable<Courses> {
    return this.http.put<Courses>(`${this.coursesUrl}/${courses.coursesId}`, courses)
  }

  delete(courses: Courses) {
    return this.http.delete(`${this.coursesUrl}/${courses.coursesId}`)
  }
}
