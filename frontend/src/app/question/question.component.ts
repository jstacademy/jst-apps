import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Question } from "../models/question.model";
import { Answer } from "../models/answer.model";

@Component({
  selector: 'question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {
  @Input() question!: Question;
  @Input() position!: number;
  @Output() answerSelected = new EventEmitter<{ question: Question, answer: Answer }>();

  onAnswerSelected(question: Question, answer: Answer) {
    this.answerSelected.emit({ question, answer });
  }
}
