import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from "@angular/router"
import { Training } from '../models/training.model';

@Component({
  selector: 'training-admin',
  standalone: false,

  templateUrl: './training-admin.component.html',
  styleUrls: ['./training-admin.component.scss'],
})
export class TrainingAdminComponent implements OnInit {
  trainings$: Observable<Training[]>;
  dataSource: Training[] = [];
  displayedColumns: string[] = ['trainingId', 'title', 'description', 'edit'];

  constructor(private _route: ActivatedRoute) {
    this.trainings$ = this._route.data.pipe(map((data) => data["trainings"] as Training[] || []));
  }

  ngOnInit(): void {
    this.trainings$.subscribe(trainings =>
      {
        this.dataSource = trainings;
      }
    );
  }
}
