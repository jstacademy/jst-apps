# JST - Academy

## Table of Contents
1. [Description](#1-description)
2. [Lancement du projet](#2-lancement-du-projet)
3. [Le projet](#3-le-projet)  
   1. [Les formations](#31-suivis-dune-formation)
   2. [Les Quiz](#32-réalisation-dun-quiz)
   3. [L'administration](#33-page-dadministration)
4. [Devops](#4-devops)
   1. [Image docker](#41-version-de-production-avec-une-image-docker)
   2. [Dockerfiles](#42-détails-des-dockerfiles)
5. [Pipeline CI/CD](#5-vue-densemble-du-pipeline-cicd)
   1. [Configuration de base](#51-configuration-de-base)
   2. [Étapes du pipeline](#52-étapes-du-pipeline)
   3. [Construction avec modèles](#53-construction-avec-modèles)
   4. [Processus de déploiement](#54-processus-de-déploiement)
   5. [Bonnes pratiques et sécurité](#55-bonnes-pratiques-et-sécurité)
6. [Contributeurs](#6-contributeurs)

## 1. Description
JST Academy est une application de formation, on y retrouve des **Parcours** composés de plusieurs **cours** et de **leçons**.
À l'issue de certaine leçon un **Quiz** peut être réalisé pour **vérifier les acquis**.
Il s'agit d'un **projet académique** dans une matière visant à **explorer les technologies Web**, ici **JavaSpring Boot** et **Angular**.
Le projet est réalisé dans le cadre de notre semestre huit en quatrième année d'école d'ingénieurs à l'ECE PARIS.

## 2. Lancement du projet
Pour démarrer le projet en DEV, suivez les étapes suivantes :

1. Copiez le fichier `.env.sample` et renommez-le en `.env`. Assurez-vous de remplir les variables nécessaires selon votre configuration locale.
2. Démarrez la base de données en utilisant Docker Compose avec la commande : `docker compose up database`.
3. Lancez le serveur frontend en exécutant `ng serve` dans le terminal. Cela démarrera le serveur de développement Angular.
4. Pour le backend, ouvrez le logiciel **IntelliJ IDEA** et configurez-le pour utiliser le fichier `.env` comme source de vos variables d'environnement, puis démarrez le serveur backend.

## 3. Le projet
La Web App est composée d'une page d'accueil, avec la liste des Trainings disponible. 
Lorsque l'on ouvre un training, on retrouve les cours sous forme de plusieurs étapes, avec les leçons et des boutons 
vers les Quiz associés.
Les pages de Quiz sont composés de plusieurs questions à choix unique. 
Toutes les questions doivent avoir une réponse avant validation. 
Une fois validé le score s'affiche avec un message personnalisé.

### 3.1. Suivis d'une formation
La page d'accueil du site affiche les formations :
![home-page](img-readme/home-page.png "Page d'accueil JST-Academy")
Les formations sont composées de plusieurs étapes (les cours), et chaque cours ont plusieurs leçons avec des Quiz 
associés pour vérifier ses acquis.
![training-page](img-readme/training/training-next.png "Page de formation")
- - - -
En fonction de votre position dans le parcours, vous avez des boutons pour retourner au précédent cours ou au suivant.
![training-ux](img-readme/training/training-next-back.png "Bouton next-back pour changer de cours")

- - - -
### 3.2. Réalisation d'un Quiz
Les quiz sont composées de plusieurs questions à choix unique.
![quiz-page](img-readme/quiz/quiz.png "Page quiz")
- - - -
Lorsque le quiz est valide et que l'utilisateur n'a pas répondu à toutes les questions une erreur s'affiche.
![quiz-error](img-readme/quiz/quiz-error.png "Page quiz avec l'affichage d'une erreur")
- - - -
Après validation du Quiz, le résultat s'affiche en bas à gauche.
L'utilisateur peut changer les réponses et le revalider pour essayer d'améliorer son score.
![quiz-score](img-readme/quiz/quiz-score.png "Page quiz avec le score")

### 3.3. Page d'administration
Tous les trainings s'affichent sous forme d'un tableau, l'utilisateur peut rajouter un training, ou faire le choix de le 
modifier.
![administration-page](img-readme/administration/administration.png "Page d'administration pour les trainings")
- - - -
La page est constituée de deux champs, pour éditer le titre du training et sa description.
Des limites sont écrites pour les différents champs.
L'utilisateur peut enregistrer ses modifications ou également faire le choix de le supprimer.
Attention la suppression supprime l'ensemble des cours, des leçons, des quiz, associés au training.
![edit-training](img-readme/administration/training-edition.png "Page administration d'édition d'un training")

- - - -

## 4. Devops

### 4.1. Version de production avec une image Docker
Nous utilisons Docker Compose pour gérer trois services principaux dans notre environnement de production : une base de données PostgreSQL, un backend Java, et un frontend sur NGINX.

**Base de données PostgreSQL :**
La base de données est configurée pour toujours redémarrer en cas d'interruption. 
Elle utilise un fichier `.env` pour ses paramètres et stocke ses données de façon permanente grâce à un volume Docker spécifié.
Le service est accessible via le port `5432` pour pouvoir l'utiliser en local.

**Backend en Java :**
Le backend fonctionne avec une image personnalisée stockée dans notre registre **GitLab**, mais peut aussi être construit localement depuis le dossier `backend/`. 
Ce service, qui attend que la base de données soit disponible avant de démarrer, utilise aussi le fichier `.env` pour ses configurations et est accessible via le port `8080`.

**Frontend sur NGINX :**
Le frontend utilise NGINX pour servir le contenu, construit à partir d'un dossier `frontend/`. 
Il est configuré pour notre domaine via des variables d'environnement et dépend du backend pour fonctionner. 
Le service est accessible via le port `80`.

### 4.2 Détails des Dockerfiles

**Backend :**
Le Dockerfile du backend est configuré pour utiliser **Maven** et construire une application **Java** empaquetée dans un fichier JAR. 
Il est optimisé pour utiliser le cache des dépendances, ce qui accélère les constructions répétées.

**Frontend :**
Pour le frontend, le processus de construction utilise **Node.js** pour préparer les fichiers statiques qui sont ensuite servis par NGINX.
Ce processus est divisé en deux étapes pour améliorer l'efficacité et la gestion du cache.

## 5. Vue d'ensemble du Pipeline CI/CD

Notre pipeline CI/CD, orchestré par **GitLab CI**, automatise le processus de build et de déploiement de nos applications backend et frontend. 
Nous utilisons **Docker** et **Ansible** pour assurer que nos applications soient déployées efficacement et de manière sécurisée dans l'environnement de production `http://justin-de-sio.fr` ou `http://13.38.87.208`.

Voici un aperçu détaillé de notre processus :
![devops-pipeline](img-readme/pipeline.png "Pipeline Gitlab pour la mise en production du projet")

### 5.1 Configuration de base

- **Image principale** : Nous utilisons l'image `docker:25` pour tous nos **jobs CI**, garantissant une compatibilité et une performance optimales.
- **Docker-in-Docker** : Le service `docker:25-dind` est utilisé pour faciliter la construction de Docker images sans interférer avec l'hôte principal, ce qui est crucial pour maintenir l'isolation et la sécurité.
- **Authentification** : Chaque session commence par une connexion au registre Docker, sécurisée par des variables d'environnement qui stockent nos identifiants.

### 5.2 Étapes du pipeline

- **Construction** : Nos images Docker pour le backend et le frontend sont construites en utilisant les meilleures pratiques de cache pour améliorer l'efficacité. Chaque service est construit séparément pour permettre une modularité et une réutilisation maximale.
- **Déploiement en production** : Le déploiement utilise Ansible pour un contrôle précis et déclaratif de l'environnement de production, s'appuyant sur des scripts automatisés pour une mise en œuvre sans heurts.

### 5.3 Construction avec modèles

- **Template de build** : Nous avons standardisé la création d'images avec un modèle réutilisable, qui gère le tirage de l'image de base, la construction de la nouvelle image, et son déploiement dans notre registre Docker.
- **Jobs de construction spécifiques** : Chaque job reprend ce modèle et l'adapte avec des variables spécifiques au contexte (par exemple, `backend` ou `frontend`), assurant que chaque partie de notre application est correctement mise à jour avec le dernier code.

### 5.4 Processus de déploiement

- **Préparation et sécurité** : Avant le déploiement, nous préparons et sécurisons les clés SSH nécessaires pour une connexion sécurisée à nos serveurs. Ces clés sont traitées de manière à éviter toute exposition directe.
- **Playbook Ansible** : Notre playbook Ansible coordonne le déploiement, depuis l'installation des dépendances jusqu'à l'exécution des conteneurs avec Docker Compose. Il assure également que toutes les anciennes instances sont correctement nettoyées avant de lancer les nouvelles.

### 5.5 Bonnes pratiques et sécurité

Nous prenons des mesures rigoureuses pour assurer la sécurité de notre pipeline, y compris l'utilisation de variables pour gérer les informations sensibles et la configuration SSH pour éliminer les interactions manuelles lors des connexions au serveur. Ce pipeline est conçu pour être aussi automatisé que possible, réduisant ainsi les erreurs humaines et accélérant le cycle de livraison.


## 6. Contributeurs
- DE-SIO Justin - [Gitlab](https://gitlab.com/Justin-De-Sio)
- ROBERT Thomas - [Gitlab](https://gitlab.com/thomas.rbrt)
- EL FEKIH Sarra
