package com.jst.backend.Course;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;


@Service
public class CourseDeserializer extends JsonDeserializer<Optional<Course>> {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseDeserializer(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public Optional<Course> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Long courseId = jsonParser.readValueAs(Long.class);
        System.out.println(jsonParser);
        return courseRepository.findById(courseId);
    }
}
