package com.jst.backend.Course;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.jst.backend.Lesson.Lesson;
import com.jst.backend.Training.Training;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "course")
@Getter
@Setter
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long courseId;

    @Column(nullable = false, length = 255)
    private String title;

    @Column(nullable = false)
    private int order_course;

    @ManyToOne
    @JoinColumn(name = "training_id", nullable = false)
    @JsonBackReference
    @JsonIgnore
    private Training training;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Lesson> lessons;

    public Course(String title, Training training, int order_course) {
        this.title = title;
        training.addCourse(this);
        this.training = training;
        this.order_course = order_course;
        this.lessons = new HashSet<>();
    }

    public Course() {
    }

    public void addLessons(Lesson lesson) {
        this.lessons.add(lesson);
    }
}
