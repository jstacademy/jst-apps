package com.jst.backend.Quiz;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.jst.backend.Lesson.Lesson;
import com.jst.backend.Question.Question;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "quiz")
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long quizId;

    @Column(nullable = false, length = 255)
    private String title;

    @Column(nullable = false, length = 2000)
    private String content;

    @OneToMany(mappedBy = "quiz", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Question> questions;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lesson_id", nullable = false)
    @JsonIgnore
    private Lesson lesson;

    public Quiz(String title, String content) {
        this.title = title;
        this.content = content;
        this.questions = new HashSet<>();
    }

    public Quiz(String title, String content, Lesson lesson) {
        this.title = title;
        this.content = content;
        this.lesson = lesson;
        lesson.setQuiz(this);
        this.questions = new HashSet<>();
    }

    public Quiz() {
    }

    public void addQuestions(Iterable<Question> questions) {
        for (Question question : questions) {
            this.addQuestion(question);
        }
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }
}
