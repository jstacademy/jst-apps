package com.jst.backend.Quiz;

import com.jst.backend.Question.Question;
import com.jst.backend.Question.QuestionDto;
import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

@Getter
public class QuizDto {
    private String title;
    private String content;
    private Long lesson;
    private Set<QuestionDto> questions;

    public Set<Question> getQuestions(Quiz quiz) {
        Set<Question> questionsList = new HashSet<>();
        for (QuestionDto questionDto : questions) {
            Question question = new Question(
                    questionDto.getQuestion(),
                    quiz
            );
            question.addAnswers(questionDto.getAnswers(question));
            questionsList.add(question);
        }
        return questionsList;
    }

    @Override
    public String toString() {
        return "QuizDto{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", lesson=" + lesson +
                ", questions=" + questions.toString() +
                '}';
    }
}
