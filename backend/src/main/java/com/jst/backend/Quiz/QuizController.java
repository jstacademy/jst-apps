package com.jst.backend.Quiz;

import com.jst.backend.Lesson.Lesson;
import com.jst.backend.Lesson.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/quiz")
@CrossOrigin(origins = "*")
public class QuizController {
    private final QuizService quizService;
    private final LessonService lessonService;

    @Autowired
    public QuizController(QuizService quizService, LessonService lessonService) {
        this.quizService = quizService;
        this.lessonService = lessonService;
    }

    public List<Quiz> getAllQuiz() {
        return quizService.getAllQuiz();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Quiz> getQuizById(@PathVariable Long id) {
        return quizService.getQuizById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/byLesson/{lessonId}")
    public ResponseEntity<List<Quiz>> getQuizByLessonId(@PathVariable Long lessonId) {
        List<Quiz> quizzes = quizService.getQuizByLessonId(lessonId);
        if (quizzes.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(quizzes);
    }

    @PostMapping()
    public ResponseEntity<Quiz> createQuiz(@RequestBody QuizDto quizDto) {
        Lesson lesson = lessonService.getLessonById(quizDto.getLesson()).orElse(null);
        if (lesson == null) {
            return ResponseEntity.notFound().build();
        }
        System.out.println(quizDto);
        Quiz quiz = new Quiz(quizDto.getTitle(), quizDto.getContent(), lesson);
        quiz.addQuestions(quizDto.getQuestions(quiz));
        quiz = quizService.saveQuiz(quiz);
        return ResponseEntity.ok(quiz);
    }

    @PostMapping("/checkAnswers/{id}")
    public ResponseEntity<Integer> checkAnswerOfOneQuiz(@PathVariable Long id, @RequestBody Long[] listAnswerId) {
        int nbGoodAnswer = quizService.checkAnswerQuiz(id, listAnswerId);
        return ResponseEntity.ok(nbGoodAnswer);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Quiz> updateQuiz(@PathVariable Long id, @RequestBody Quiz quiz) {
        System.out.println(quiz);
        return ResponseEntity.ok(quizService.updateQuiz(id, quiz));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteQuiz(@PathVariable Long id) {
        if (quizService.getQuizById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        quizService.deleteQuiz(id);
        return ResponseEntity.ok().build();
    }
}
