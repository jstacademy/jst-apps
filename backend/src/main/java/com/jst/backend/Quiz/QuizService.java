package com.jst.backend.Quiz;

import com.jst.backend.Answer.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class QuizService {

    private final QuizRepository quizRepository;
    private final AnswerService answerService;

    @Autowired
    public QuizService(QuizRepository quizRepository, AnswerService answerService) {
        this.quizRepository = quizRepository;
        this.answerService = answerService;
    }

    public List<Quiz> getAllQuiz() {
        return quizRepository.findAll();
    }

    public Optional<Quiz> getQuizById(Long id) {
        return quizRepository.findById(id);
    }

    public List<Quiz> getQuizByLessonId(Long lessonId) {
        return quizRepository.findQuizByLessonLessonId(lessonId);
    }

    @Transactional
    public Quiz saveQuiz(Quiz quiz) {
        return quizRepository.save(quiz);
    }

    @Transactional
    public Quiz updateQuiz(Long id, Quiz quizParam) {
        System.out.println(id);
        Quiz quiz = quizRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException(
                        "quiz with the id " + id + "does not exist !"
                ));
        if (quizParam.getTitle() != null &&
                quizParam.getTitle().length() > 2 &&
                !Objects.equals(quiz.getTitle(), quizParam.getTitle())) {
            quiz.setTitle(quizParam.getTitle());
        }
        if (quizParam.getContent() != null &&
                quizParam.getContent().length() > 10 &&
                !Objects.equals(quiz.getContent(), quizParam.getContent())) {
            quiz.setContent(quizParam.getContent());
        }
        if (quizParam.getLesson() != null &&
                !Objects.equals(quiz.getLesson(), quizParam.getLesson())) {
            quiz.setLesson(quizParam.getLesson());
        }
        return quizRepository.save(quiz);
    }

    @Transactional
    public void deleteQuiz(Long id) {
        quizRepository.deleteById(id);
    }

    @Transactional
    public Integer checkAnswerQuiz(Long idQuiz, Long[] listAnswerId) {
        Optional<Quiz> quiz = quizRepository.findById(idQuiz);
        if (quiz.isEmpty()) {
            throw new IllegalStateException("quiz with the id " + idQuiz + "does not exist !");
        } else {
            return answerService.checkAnswers(quiz.get(), listAnswerId);
        }
    }
}
