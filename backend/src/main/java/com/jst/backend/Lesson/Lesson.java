package com.jst.backend.Lesson;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.jst.backend.Course.Course;
import com.jst.backend.Quiz.Quiz;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Table(name = "lesson")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long lessonId;

    @Setter
    @Column(nullable = false, length = 255)
    private String title;

    @Setter
    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    @Setter
    @Getter
    @Column(nullable = false)
    private int order_lesson;

    @Setter
    @JsonBackReference
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "course_id", nullable = false)
    private Course course;

    @OneToOne(mappedBy = "lesson", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    @JsonManagedReference
    private Quiz quiz;

    public Lesson(String title, String content, Course course, int order) {
        this.title = title;
        this.content = content;
        this.course = course;
        this.order_lesson = order;
        course.addLessons(this);
    }

    public Lesson(String title, String content, Course course, Quiz quiz) {
        this.title = title;
        this.content = content;
        this.course = course;
        this.quiz = quiz;
    }

    public Lesson() {
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
        quiz.setLesson(this);
    }
}
