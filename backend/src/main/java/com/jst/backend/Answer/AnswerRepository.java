package com.jst.backend.Answer;

import com.jst.backend.Quiz.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    List<Answer> findAnswerByQuestionQuestionId(Long questionId);
    Answer findAnswerByQuestion_QuizAndAnswerId(Quiz quiz,Long answerId);
}
