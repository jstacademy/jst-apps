package com.jst.backend.Answer;

import com.jst.backend.Question.Question;
import com.jst.backend.Question.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/answer")
@CrossOrigin(origins = "*")
public class AnswerController {
    private final AnswerService answerService;
    private final QuestionService questionService;

    @Autowired
    public AnswerController(AnswerService answerService, QuestionService questionService) {
        this.answerService = answerService;
        this.questionService = questionService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Answer> getAnswerById(@PathVariable Long id) {
        return ResponseEntity.ok(answerService.getAnswerById(id));
    }

    @GetMapping("/byQuestion/{questionId}")
    public ResponseEntity<List<Answer>> getAnswerByQuestionId(@PathVariable Long questionId) {
        List<Answer> answers = answerService.getAnswerByQuestionId(questionId);
        if (answers.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(answers);
    }

    @PostMapping()
    public ResponseEntity<Object> createAnswer(@RequestBody AnswerDto answerDto) {
        Question question = questionService.getQuestionById(answerDto.getQuestion()).orElse(null);
        if (question == null) {
            return ResponseEntity.notFound().build();
        }
        Answer answer = new Answer(answerDto.getAnswer(), answerDto.isTrue(), question);
        answer = answerService.saveAnswer(answer);
        return ResponseEntity.ok(answer);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Answer> updateAnswer(@PathVariable Long id, @RequestBody Answer answer) {
        return ResponseEntity.ok(answerService.updateAnswer(id, answer));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAnswer(@PathVariable Long id) {
        if (answerService.getAnswerById(id) == null) {
            return ResponseEntity.notFound().build();
        }
        answerService.deleteAnswer(id);
        return ResponseEntity.ok().build();
    }
}
