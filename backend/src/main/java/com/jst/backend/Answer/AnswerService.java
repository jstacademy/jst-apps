package com.jst.backend.Answer;

import com.jst.backend.Exception.ResourceNotFoundException;
import com.jst.backend.Quiz.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class AnswerService {

    private final AnswerRepository answerRepository;

    @Autowired
    public AnswerService(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    public Answer getAnswerById(Long id) {
        Optional<Answer> answer = answerRepository.findById(id);
        if (answer.isPresent()) {
            return answer.get();
        } else {
            throw new ResourceNotFoundException(
                    "Answer with the id " + id + " does not exist !"
            );
        }
    }

    public List<Answer> getAnswerByQuestionId(Long questionId) {
        return answerRepository.findAnswerByQuestionQuestionId(questionId);
    }

    public Answer saveAnswer(Answer answer) {
        return answerRepository.save(answer);
    }

    @Transactional
    public Answer updateAnswer(Long id, Answer answerParam) {
        Answer answer = getAnswerById(id);
        if (answerParam.getAnswer() != null &&
                answerParam.getAnswer().length() > 2 &&
                !Objects.equals(answer.getAnswer(), answerParam.getAnswer())) {
            answer.setAnswer(answerParam.getAnswer());
        }
        if (answerParam.getQuestion() != null) {
            answer.setQuestion(answerParam.getQuestion());
        }
        answer.setTrue(answerParam.isTrue());
        return answerRepository.save(answer);
    }

    public void deleteAnswer(Long id) {
        answerRepository.deleteById(id);
    }

    public int checkAnswers(Quiz quiz, Long[] listAnswer) {
        int nbGoodAnswer = 0;
        if (listAnswer.length == 0)
            return -1;
        for (Long answerId : listAnswer) {
            Answer answer = answerRepository.findAnswerByQuestion_QuizAndAnswerId(quiz, answerId);
            if (answer == null)
                continue;
            if (answer.isTrue())
                nbGoodAnswer++;
        }
        return nbGoodAnswer;
    }
}
