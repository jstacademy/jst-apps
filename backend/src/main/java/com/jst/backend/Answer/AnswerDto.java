package com.jst.backend.Answer;

import jakarta.annotation.Nullable;
import lombok.Getter;

@Getter
public class AnswerDto {
    private String answer;
    private boolean isTrue;
    @Nullable
    private Long question;

    @Override
    public String toString() {
        return "AnswerDto{" +
                "answer='" + answer + '\'' +
                ", isTrue=" + isTrue +
                ", question=" + question +
                '}';
    }
}
