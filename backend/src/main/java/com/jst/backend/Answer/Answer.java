package com.jst.backend.Answer;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jst.backend.Question.Question;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "answer")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long answerId;

    @Column(nullable = false, length = 255)
    private String answer;

    @Column(nullable = false)
    private boolean isTrue;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "question_id", nullable = false)
    @JsonIgnore
    private Question question;

    public Answer(String answer, boolean isTrue, Question question) {
        this.answer = answer;
        this.isTrue = isTrue;
        this.question = question;
        question.addAnswer(this);
    }

    public Answer() {
    }
}
