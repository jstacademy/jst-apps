package com.jst.backend.Training;

import com.jst.backend.Answer.Answer;
import com.jst.backend.Course.Course;
import com.jst.backend.Course.CourseRepository;
import com.jst.backend.Lesson.Lesson;
import com.jst.backend.Lesson.LessonRepository;
import com.jst.backend.Question.Question;
import com.jst.backend.Quiz.Quiz;
import com.jst.backend.Quiz.QuizRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class TrainingConfig {
    @Bean
    CommandLineRunner commandLineRunner(TrainingRepository trainingRepository,
                                        CourseRepository courseRepository,
                                        LessonRepository lessonRepository,
                                        QuizRepository quizRepository) {

        return args -> {
            loadHTMLData(trainingRepository,
                    courseRepository,
                    lessonRepository,
                    quizRepository
            );

            loadJavaData(trainingRepository,
                    courseRepository,
                    lessonRepository,
                    quizRepository
            );

            loadPythonData(trainingRepository,
                    quizRepository
            );

            loadSQLData(trainingRepository,
                    courseRepository,
                    lessonRepository
            );

            loadSymfonyData(trainingRepository,
                    quizRepository);
        };
    }

    public void loadHTMLData(TrainingRepository trainingRepository,
                             CourseRepository courseRepository,
                             LessonRepository lessonRepository,
                             QuizRepository quizRepository) {
        Training html = new Training("Comprendre le language HTML et comment créer une page web simple", "Vous allez découvrir les bases de HTML, un langage essentiel pour la création de pages web. Vous allez explorer la structure de base d'un document HTML, les balises fondamentales, et même créé votre propre page web simple. Cependant, il reste encore beaucoup à apprendre. Dans les leçons à venir, vous explorerez davantage les éléments sémantiques, l'ajout de liens, de médias, de formulaires interactifs, et même les bases du stylisme avec CSS. Ces connaissances vous aideront à construire des sites web plus complexes et esthétiquement attrayants. Alors détendez-vous, rechargez vos batteries et préparez-vous à plonger dans les leçons suivantes avec une énergie renouvelée !");

        Course htmlIntroduction = new Course("Introduction", html, 1);
        Course htmlIntroductionCSS = new Course("Introduction à CSS (Cascading Style Sheets)", html, 2);

        Lesson htmlIntroductionQuestCeQueHTML = new Lesson("Qu'est-ce que HTML?",
                """
                          - HTML (HyperText Markup Language) est le langage de balisage standard utilisé pour créer et structurer le contenu des pages web.
                           - Il permet de définir la structure logique et le contenu d'une page web en utilisant des balises.

                        Structure de base d'un document HTML (balises, éléments, attributs)
                           - Un document HTML est composé d'une structure de base qui inclut des balises, des éléments et des attributs.
                           - Les balises sont utilisées pour marquer le début et la fin des éléments dans une page web.
                           - Les éléments sont constitués de balises et du contenu qu'elles encadrent.
                           - Les attributs fournissent des informations supplémentaires sur les éléments et sont spécifiés à l'intérieur des balises.

                        Les balises les plus courantes
                           - <html>: Indique le début et la fin du document HTML.
                           - <head>: Contient les informations sur le document telles que le titre, les métadonnées, etc.
                           - <title>: Définit le titre de la page qui s'affiche dans la barre de titre du navigateur.
                           - <body>: Contient tout le contenu visible de la page web.
                           - D'autres balises courantes incluent <h1> à <h6> pour les titres, <p> pour les paragraphes, <img> pour les images, etc.

                        Création d'une page web simple avec un titre, un paragraphe et une image
                           - Pour créer une page web simple, commencez par définir la structure de base du document en utilisant les balises <html>, <head> et <body>.
                           - Utilisez la balise <title> pour définir le titre de la page.
                           - À l'intérieur du corps de la page (<body>), utilisez les balises <h1> à <h6> pour les titres, <p> pour les paragraphes et <img> pour les images.
                           - Utilisez les attributs appropriés pour spécifier les informations supplémentaires, comme l'attribut src pour définir l'URL de l'image dans la balise <img>.""", htmlIntroduction, 1);

        Lesson htmlIntroductionBalises = new Lesson("Structurer ça page avec des balises",
                """
                        L'importance de la sémantique en HTML
                        La sémantique en HTML se réfère à l'utilisation appropriée des balises pour décrire le contenu d'une page de manière significative pour les moteurs de recherche, les lecteurs d'écran et les développeurs.
                        Une structure sémantique claire améliore l'accessibilité, le référencement et la compréhension du contenu pour les utilisateurs.
                        Utilisation des balises sémantiques
                        HTML5 introduit un ensemble de balises sémantiques qui décrivent le contenu de manière plus précise.
                        <header>: Utilisé pour le contenu introductif ou de tête d'une page ou d'une section.
                        <nav>: Pour les liens de navigation principaux d'une page.
                        <section>: Définit une section générique d'une page.
                        <article>: Pour le contenu autonome qui pourrait être réutilisé ou syndiqué.
                        <footer>: Contient les informations de pied de page d'une page ou d'une section.
                        Création d'une structure de page web avec un en-tête, une barre de navigation, des sections et un pied de page
                        Commencez par inclure les balises <header> et <footer> pour définir les parties d'en-tête et de pied de page de votre page.
                        Utilisez la balise <nav> pour regrouper les liens de navigation principaux.
                        À l'intérieur du corps de la page, utilisez les balises <section> et <article> pour organiser le contenu de manière logique.
                        Chaque section ou article peut avoir son propre en-tête et/ou pied de page si nécessaire, créant ainsi une structure hiérarchique.
                        """, htmlIntroduction, 2);

        Lesson htmlIntroductionLienImageFormulaire = new Lesson("Utilisation de liens, image et Formulaire",
                """
                        Utilisation de la balise <a> pour créer des liens internes et externes
                        La balise <a> (pour "anchor") est utilisée pour créer des liens hypertextes vers d'autres pages web, des fichiers, des adresses email, des sections d'une même page (ancres), etc.
                        Pour créer un lien vers une URL externe, utilisez l'attribut "href" avec la valeur de l'URL cible. Par exemple : <a href="https://www.example.com">Visiter Example.com</a>.
                        Pour créer un lien interne vers une autre section de la même page, utilisez l'attribut "href" avec le nom de l'ancre. Par exemple : <a href="#section2">Aller à la section 2</a>.
                        Incorporation d'images avec la balise <img>
                        La balise <img> est utilisée pour incorporer des images dans une page web.
                        L'attribut "src" est utilisé pour spécifier l'URL de l'image à afficher.
                        Les attributs "alt" (texte alternatif) et "title" (info-bulle) peuvent être utilisés pour fournir une description de l'image à des fins d'accessibilité et de référencement.
                        Ajout de vidéos et d'audio avec les balises <video> et <audio>
                        La balise <video> est utilisée pour incorporer des vidéos dans une page web.
                        L'attribut "src" est utilisé pour spécifier l'URL de la vidéo à afficher.
                        Les attributs "controls" ajoutent des contrôles de lecture à la vidéo (lecture, pause, volume, etc.).
                        La balise <audio> est utilisée de manière similaire pour incorporer des fichiers audio dans une page web.
                        Formulaires HTML avec la balise <form>
                        Les formulaires HTML permettent aux utilisateurs d'entrer des données interactives sur une page web, telles que des informations de contact, des commentaires, des commandes, etc.
                        La balise <form> est utilisée pour définir un formulaire HTML et encapsuler les éléments interactifs à l'intérieur.
                        Utilisation des balises <input>, <textarea>, <select> pour différents types de saisie
                        La balise <input> est la plus couramment utilisée pour permettre aux utilisateurs de saisir des données. Elle peut être configurée avec différents types d'entrées, comme du texte, des cases à cocher, des boutons radio, des champs de saisie de date, etc.
                        La balise <textarea> est utilisée pour permettre aux utilisateurs de saisir des blocs de texte plus longs, tels que des commentaires ou des messages.
                        La balise <select> crée un menu déroulant à partir duquel les utilisateurs peuvent sélectionner une option parmi plusieurs.
                        Ajout de boutons d'envoi avec la balise <button>
                        La balise <button> est utilisée pour créer des boutons interactifs dans un formulaire, tels que des boutons de soumission, d'annulation ou de réinitialisation.
                        Vous pouvez également utiliser la valeur de l'attribut type pour spécifier le comportement du bouton, comme "submit" pour soumettre le formulaire, "reset" pour réinitialiser les valeurs du formulaire, ou "button" pour un bouton standard.
                        """, htmlIntroduction, 3);

        Lesson htmlIntroductionQuestCeQueCSS = new Lesson("Qu'est-ce que CSS ?",
                """
                        CSS, acronyme de Cascading Style Sheets, est un langage de feuilles de style utilisé pour contrôler l'apparence et la mise en forme des éléments HTML sur une page web. Il offre une méthode efficace pour styliser et mettre en page le contenu d'un site web, séparant ainsi la présentation visuelle du contenu structurel. Voici une détaillée de ce que représente CSS et pourquoi il est essentiel dans le développement web :
                        Qu'est-ce que CSS ?
                        CSS permet de définir des règles de style pour différents éléments HTML, ce qui signifie qu'il peut contrôler des aspects tels que la couleur, la taille du texte, la police, la disposition, les marges, les bordures et bien plus encore. Grâce à CSS, les concepteurs et développeurs web peuvent apporter des modifications visuelles cohérentes et flexibles à leurs pages web, offrant ainsi une expérience utilisateur plus agréable et professionnelle.
                        Pourquoi utiliser CSS ?
                        Séparation des préoccupations : CSS permet de séparer la structure (HTML) de la présentation (CSS) et du comportement (JavaScript) d'une page web. Cette séparation des préoccupations favorise une organisation plus claire du code et facilite la maintenance du site.
                        Flexibilité et contrôle : CSS offre un contrôle granulaire sur l'apparence des éléments d'une page web. Les concepteurs peuvent modifier facilement l'apparence de l'ensemble du site ou de sections spécifiques en ajustant simplement les règles CSS, sans avoir à modifier le contenu HTML.
                        Consistance et cohérence : En appliquant des styles cohérents à travers toutes les pages d'un site web, CSS garantit une expérience utilisateur uniforme et professionnelle. Les styles peuvent être réutilisés et centralisés dans des fichiers externes, ce qui facilite leur gestion et leur mise à jour.
                        Syntaxe de base de CSS
                        Une règle CSS est généralement composée de deux parties : le sélecteur et la déclaration.
                        Sélecteur : Identifie les éléments HTML auxquels la règle s'applique. Il peut s'agir d'un élément spécifique, d'une classe, d'un ID ou d'une combinaison de ceux-ci.
                        Déclaration : Comprend une ou plusieurs propriétés CSS, chacune avec une valeur spécifique. Les propriétés définissent les styles à appliquer aux éléments ciblés.
                        En résumé, CSS est un outil essentiel pour créer des sites web attrayants et fonctionnels. Il offre un contrôle précis sur l'apparence des éléments HTML et facilite la création de designs cohérents et professionnels à travers toutes les pages d'un site.
                        """, htmlIntroductionCSS, 1);

        Lesson htmlIntroductionStyleDeBase = new Lesson("CSS application de styles de base",
                """
                        Balise `<style>
                           - Les styles CSS peuvent être définis directement dans une balise `<style>` placée dans l'en-tête de la page HTML.
                           - Cette méthode est utile pour des styles spécifiques à une seule page et ne nécessitant pas de réutilisation sur d'autres pages.
                           - Les règles CSS sont écrites entre les balises `<style>` et `</style>` et suivent la même syntaxe que dans un fichier CSS externe.

                        Fichier externe CSS
                           - Les styles CSS peuvent également être placés dans un fichier externe avec l'extension .css.
                           - Ce fichier CSS peut ensuite être inclus dans une page HTML à l'aide de la balise `<link>` située dans l'en-tête de la page.
                           - Cette méthode est recommandée pour les styles réutilisables sur plusieurs pages ou pour une gestion plus organisée des styles.


                        Utilisation des sélecteurs CSS pour cibler des éléments HTML

                        Types de sélecteurs CSS
                           - Les sélecteurs CSS permettent de cibler spécifiquement les éléments HTML auxquels appliquer des styles.
                           - Les types de sélecteurs comprennent les sélecteurs de type, les sélecteurs de classe, les sélecteurs d'ID, les sélecteurs de descendant, etc.
                           - Les sélecteurs de classe et d'ID offrent une manière plus précise de cibler des éléments spécifiques, tandis que les sélecteurs de type ciblent tous les éléments d'un type donné.

                        Application de styles de base

                        Modification de l'apparence des éléments HTML
                           - Une fois que vous avez ciblé les éléments HTML avec des sélecteurs CSS, vous pouvez leur appliquer différents styles pour modifier leur apparence.
                           - Les styles de base comprennent la couleur du texte, la taille du texte, la famille de polices, les marges, les rembourrages, les bordures, etc.

                        En appliquant ces méthodes et principes de base, vous pouvez commencer à styliser efficacement vos pages web avec CSS, offrant ainsi une expérience visuelle attrayante et cohérente à vos utilisateurs.

                        En résumé, cette formation a permis de comprendre les bases du développement web avec HTML et CSS. Nous avons appris à structurer le contenu avec HTML, à styliser les pages avec CSS, et à les rendre interactives\s
                        avec des liens, des médias et des formulaires. En utilisant différentes méthodes pour inclure du CSS et en appliquant des styles de base, nous avons acquis les compétences nécessaires pour créer des sites web statiques\s
                        attrayants et fonctionnels. Cette formation constitue une base solide pour explorer davantage le développement web et continuer à perfectionner nos compétences dans ce domaine en constante évolution.""", htmlIntroductionCSS, 2);


        Quiz htmlQuestcequeHTML = new Quiz("Évaluation de Connaissances HTML",
                """
                        Ce quiz vise à tester vos connaissances de base sur HTML (HyperText Markup Language), le langage de balisage standard utilisé pour créer et structurer le contenu des pages web. Il comprend une série de questions à choix unique portant sur les concepts fondamentaux de HTML, y compris le rôle des balises, la structure d'un document HTML, et les éléments essentiels d'une page web.")
                        """, htmlIntroductionQuestCeQueHTML);


        Question Question1 = new Question("Qu'est-ce que HTML ?", htmlQuestcequeHTML);

        Answer Answer1 = new Answer("Un langage de balisage",
                true, Question1);
        Answer Answer1_1 = new Answer("Un langage de programmation",
                false, Question1);
        Answer Answer1_2 = new Answer("Un framework de développement web",
                false, Question1);
        Answer Answer1_3 = new Answer("Un système de gestion de base de données",
                false, Question1);

        Question Question2 = new Question("Quel est le rôle des balises HTML ?", htmlQuestcequeHTML);

        Answer Answer2 = new Answer(" Structurer et marquer le contenu d'une page web",
                true, Question2);
        Answer Answer2_1 = new Answer("Définir le style visuel d'une page web",
                false, Question2);
        Answer Answer2_2 = new Answer("Exécuter des scripts côté serveur",
                false, Question2);
        Answer Answer2_3 = new Answer("Définir des fonctions interactives pour les utilisateurs",
                false, Question2);


        Question Question3 = new Question("Quel élément HTML est utilisé pour définir le titre d'une page web ?", htmlQuestcequeHTML);

        Answer Answer3 = new Answer("<title>",
                true, Question3);
        Answer Answer3_1 = new Answer("<head>",
                false, Question3);
        Answer Answer3_2 = new Answer("<body>",
                false, Question3);
        Answer Answer3_3 = new Answer("<html>",
                false, Question3);

        Question Question4 = new Question("Quel élément HTML est utilisé pour spécifier le contenu principal d'une page web ?", htmlQuestcequeHTML);

        Answer Answer4 = new Answer("<body>",
                true, Question4);
        Answer Answer4_1 = new Answer("<head>",
                false, Question4);
        Answer Answer4_2 = new Answer("<title>",
                false, Question4);
        Answer Answer4_3 = new Answer("<html>",
                false, Question4);


        Quiz htmlSemantiqueHTML = new Quiz("Evaluation de Sémantique en HTML",
                """
                        Ce quiz "Compréhension de la Sémantique en HTML" évalue votre connaissance des concepts fondamentaux de la sémantique en HTML, ainsi que de l'utilisation appropriée des balises pour structurer le contenu d'une page web de manière significative. Il comprend une série de questions à choix multiples qui couvrent les principes de base de la sémantique en HTML et ses avantages pour l'accessibilité, le référencement et la compréhension du contenu.
                        """, htmlIntroductionBalises);

        Question Question5 = new Question("Qu'est-ce que la sémantique en HTML ?", htmlSemantiqueHTML);

        Answer Answer5 = new Answer("L'utilisation des balises pour décrire le contenu d'une page de manière significative",
                true, Question5);
        Answer Answer5_1 = new Answer("L'utilisation appropriée des balises pour styliser une page web",
                false, Question5);
        Answer Answer5_2 = new Answer("L'ajout de fonctionnalités interactives à une page web",
                false, Question5);
        Answer Answer5_3 = new Answer("La création de graphiques et d'animations en utilisant des balises spéciales",
                false, Question5);

        Question Question6 = new Question("Quels sont les avantages d'une structure sémantique en HTML ?", htmlSemantiqueHTML);

        Answer Answer6 = new Answer("Amélioration de la vitesse de chargement de la page",
                false, Question6);
        Answer Answer6_1 = new Answer("Meilleure accessibilité, référencement et compréhension du contenu",
                true, Question6);
        Answer Answer6_2 = new Answer("Réduction de la taille des fichiers HTML",
                false, Question6);
        Answer Answer6_3 = new Answer("Augmentation du nombre de visiteurs sur le site web",
                false, Question6);

        Question Question7 = new Question("Quelle balise HTML est utilisée pour le contenu introductif ou de tête d'une page ou d'une section ?", htmlSemantiqueHTML);

        Answer Answer7 = new Answer("<intro>",
                false, Question7);
        Answer Answer7_1 = new Answer("<header>",
                true, Question7);
        Answer Answer7_2 = new Answer("<section>",
                false, Question7);
        Answer Answer7_3 = new Answer("<main>",
                false, Question7);

        Question Question8 = new Question("Quelle balise HTML est utilisée pour regrouper les liens de navigation principaux d'une page ?", htmlSemantiqueHTML);

        Answer Answer8 = new Answer("<nav>",
                true, Question8);
        Answer Answer8_1 = new Answer("<header>",
                false, Question8);
        Answer Answer8_2 = new Answer("<section>",
                false, Question8);
        Answer Answer8_3 = new Answer("<footer>",
                false, Question8);


        Quiz htmlLienImageFormulaire = new Quiz("Evaluation des Balises HTML",
                """
                        Ce quiz "Compréhension des Balises HTML" évalue votre connaissance des balises HTML couramment utilisées pour créer des liens, afficher des images, incorporer des médias et créer des formulaires dans une page web. Il comprend une série de questions à choix multiples qui couvrent les concepts fondamentaux de l'utilisation des balises HTML pour diverses tâches.
                        """, htmlIntroductionLienImageFormulaire);

        Question Question9 = new Question("Comment créez-vous un lien vers une URL externe dans HTML ?", htmlLienImageFormulaire);

        Answer Answer9 = new Answer("En utilisant la balise <a> avec l'attribut href",
                true, Question9);
        Answer Answer9_1 = new Answer("En utilisant la balise <url>",
                false, Question9);
        Answer Answer9_2 = new Answer("En utilisant la balise <link>",
                false, Question9);
        Answer Answer9_3 = new Answer("En utilisant la balise <href>",
                false, Question9);

        Question Question10 = new Question(" Quel attribut est utilisé pour spécifier l'URL de l'image à afficher avec la balise <img> ?", htmlLienImageFormulaire);

        Answer Answer10 = new Answer("src",
                true, Question10);
        Answer Answer10_1 = new Answer("url",
                false, Question10);
        Answer Answer10_2 = new Answer("link",
                false, Question10);
        Answer Answer10_3 = new Answer("href",
                false, Question10);

        Question Question11 = new Question("Quelle balise est utilisée pour incorporer des vidéos dans une page web en HTML ? ", htmlLienImageFormulaire);

        Answer Answer11 = new Answer("<video>",
                true, Question11);
        Answer Answer11_1 = new Answer("<img>",
                false, Question11);
        Answer Answer11_2 = new Answer("<a>",
                false, Question11);
        Answer Answer11_3 = new Answer("<audio>",
                false, Question11);

        Question Question12 = new Question(" Quelle balise est utilisée pour encapsuler les éléments interactifs d'un formulaire HTML ?", htmlLienImageFormulaire);

        Answer Answer12 = new Answer("<form>",
                true, Question12);
        Answer Answer12_1 = new Answer("<input>",
                false, Question12);
        Answer Answer12_2 = new Answer("<button>",
                false, Question12);
        Answer Answer12_3 = new Answer("<select>",
                false, Question12);


        Quiz htmlQuestCeQueCSS = new Quiz("Evaluation Introduction CSS",
                """
                        Ce quiz portant sur "Qu'est-ce que CSS ?" évalue votre compréhension du langage CSS (Cascading Style Sheets) utilisé pour styliser les éléments HTML sur une page web. Il comprend une série de questions à choix multiples couvrant les concepts fondamentaux de CSS, son utilité et ses fonctionnalités.
                        """, htmlIntroductionQuestCeQueCSS);

        Question Question13 = new Question(" Qu'est-ce que CSS ? ", htmlQuestCeQueCSS);

        Answer Answer13 = new Answer("Un langage de programmation",
                false, Question13);
        Answer Answer13_1 = new Answer("Un langage de feuilles de style",
                true, Question13);
        Answer Answer13_2 = new Answer("Un langage de balisage",
                false, Question13);
        Answer Answer13_3 = new Answer("Un langage de base de données",
                false, Question13);

        Question Question14 = new Question("Quel est l'acronyme de CSS ? ", htmlQuestCeQueCSS);

        Answer Answer14 = new Answer("Cascading Style Sheets",
                true, Question14);
        Answer Answer14_1 = new Answer("Creative Style Sheets",
                false, Question14);
        Answer Answer14_2 = new Answer("Central Style Sheets",
                false, Question14);
        Answer Answer14_3 = new Answer("Combined Style Sheets",
                false, Question14);

        Question Question15 = new Question("Pourquoi utiliser CSS ? ", htmlQuestCeQueCSS);

        Answer Answer15 = new Answer("Pour séparer la structure, la présentation et le comportement d'une page web",
                true, Question15);
        Answer Answer15_1 = new Answer("Pour contrôler la structure des éléments HTML",
                false, Question15);
        Answer Answer15_2 = new Answer("Pour gérer les interactions côté serveur",
                false, Question15);
        Answer Answer15_3 = new Answer("Pour manipuler les données de la base de données",
                false, Question15);

        Question Question16 = new Question("Qu'est-ce que signifie 'CSS' dans 'Cascading Style Sheets' ? ", htmlQuestCeQueCSS);

        Answer Answer16 = new Answer("Feuilles de style en cascade",
                true, Question16);
        Answer Answer16_1 = new Answer("Hiérarchie de style",
                false, Question16);
        Answer Answer16_2 = new Answer("Mise en forme en cascade",
                false, Question16);
        Answer Answer16_3 = new Answer("Stylisation en cascade",
                false, Question16);


        trainingRepository.save(html);
        courseRepository.saveAll(List.of(htmlIntroduction, htmlIntroductionCSS));
        lessonRepository.saveAll(List.of(htmlIntroductionLienImageFormulaire,
                htmlIntroductionBalises,
                htmlIntroductionStyleDeBase,
                htmlIntroductionQuestCeQueHTML, htmlIntroductionQuestCeQueCSS));
        quizRepository.saveAll(List.of(htmlLienImageFormulaire,
                htmlSemantiqueHTML,
                htmlQuestcequeHTML,
                htmlQuestCeQueCSS));


    }

    public void loadJavaData(TrainingRepository trainingRepository,
                             CourseRepository courseRepository,
                             LessonRepository lessonRepository,
                             QuizRepository quizRepository) {
        Training java = new Training("Débuter en Java", "Bienvenue dans ce cours passionnant qui vous plongera dans le vaste univers de la programmation en Java et de l'algorithmie ! Que vous soyez un débutant enthousiaste ou un programmeur aguerri en quête d'approfondissement, ce cours vous offre une expérience d'apprentissage enrichissante et captivante.");

        Course javaIntroduction = new Course("Introduction", java, 1);

        Lesson javaIntroductionJava = new Lesson("Introduction à Java", """
                Java est l'un des langages de programmation les plus populaires et polyvalents utilisés dans le développement de logiciels. Dans ce chapitre introductif, nous explorerons les bases de Java, son histoire et ses principaux avantages, ainsi que les étapes pour installer et configurer votre environnement de développement Java. Enfin, nous créerons et exécuterons notre première application Java pour nous familiariser avec la syntaxe de base.
                1.1 Qu'est-ce que Java ?
                - Historique de Java
                Java a été développé par Sun Microsystems, Inc. (acquis plus tard par Oracle Corporation) dans les années 1990. James Gosling est souvent crédité comme le père fondateur de Java. Le langage a été conçu pour être portable, orienté objet et sécurisé, avec la possibilité de fonctionner sur différents types de plates-formes sans nécessiter de modifications majeures.
                - Pourquoi utiliser Java ?
                Java est largement utilisé dans de nombreux domaines, notamment le développement d'applications web, mobiles, d'entreprise et embarquées. Voici quelques-unes des raisons pour lesquelles Java est choisi :
                Portabilité : Les programmes Java peuvent être exécutés sur n'importe quel appareil qui dispose d'une machine virtuelle Java (JVM), ce qui rend les applications Java facilement distribuables sur différents systèmes d'exploitation.
                Orienté objet : Java est un langage orienté objet, ce qui signifie qu'il favorise la modularité, la réutilisabilité et la maintenabilité du code.
                Sécurité : Java intègre des fonctionnalités de sécurité telles que la gestion automatique de la mémoire et la vérification des types, ce qui contribue à réduire les risques de failles de sécurité.
                1.2 Installation de l'environnement de développement
                - Téléchargement et installation de Java Development Kit (JDK)
                Pour commencer à programmer en Java, vous devez installer le JDK, qui comprend le compilateur Java (javac) et d'autres outils nécessaires pour développer des applications Java. Vous pouvez télécharger le JDK à partir du site officiel d'Oracle et suivre les instructions d'installation pour votre système d'exploitation.
                - Configuration de l'IDE (Integrated Development Environment)
                Un IDE est un outil essentiel pour le développement Java, car il fournit un environnement de travail convivial avec des fonctionnalités telles que l'édition de code, le débogage et la gestion de projets. Des IDE populaires pour Java incluent Eclipse, IntelliJ IDEA et NetBeans. Après avoir installé votre IDE de choix, configurez-le en spécifiant le chemin vers le JDK installé.
                1.3 Première application Java
                - Structure d'un programme Java
                Un programme Java est organisé en classes, qui sont les blocs de construction fondamentaux de tout programme Java. Chaque programme Java commence par une classe contenant une méthode spéciale appelée main(), qui est le point d'entrée du programme. À l'intérieur de la méthode main(), vous écrivez le code à exécuter.
                - Compilation et exécution d'un programme Java
                Une fois que vous avez écrit votre code Java, vous devez le compiler en bytecode Java à l'aide du compilateur javac. Ensuite, vous pouvez exécuter le bytecode en utilisant la commande java, en spécifiant le nom de la classe contenant la méthode main(). Cela lancera votre programme Java.
                - Introduction aux commentaires et à la syntaxe de base
                Java prend en charge les commentaires pour documenter votre code. Les commentaires peuvent être de deux types : les commentaires à une ligne (//) et les commentaires multi-lignes (/* ... */). La syntaxe de base de Java comprend des règles pour déclarer des variables, appeler des méthodes, utiliser des opérateurs et plus encore.""", javaIntroduction, 1);

        Lesson javaIntroductionBase = new Lesson("Les bases du langage Java",
                """
                        Dans ce chapitre, nous plongerons dans les bases du langage Java, en commençant par les types de données et les variables, en passant par les opérateurs, les structures de contrôle et enfin les tableaux.
                        2.1 Types de données et variables
                        - Types primitifs (int, double, boolean, etc.)
                        Java prend en charge plusieurs types de données primitifs, tels que les entiers (int), les nombres à virgule flottante (double), les booléens (boolean), les caractères (char), etc. Ces types primitifs sont les éléments de base pour stocker des données en Java.
                        - Déclaration et initialisation des variables
                        Vous pouvez déclarer une variable en spécifiant son type et son nom. L'initialisation d'une variable se fait en lui assignant une valeur. Par exemple :
                        java

                        - Portée des variables
                        La portée d'une variable détermine où elle peut être utilisée dans le code. Les variables peuvent avoir une portée locale, limitée à un bloc de code, ou une portée globale, accessible dans tout le programme.
                        2.2 Opérateurs
                        - Opérateurs arithmétiques, de comparaison et logiques
                        Java prend en charge divers opérateurs arithmétiques tels que +, -, *, /, etc., des opérateurs de comparaison tels que ==, !=, >, <, etc., et des opérateurs logiques tels que &&, ||, !, etc.
                        - Opérateurs d'assignation et d'incrémentation/décrémentation
                        Les opérateurs d'assignation tels que =, +=, -= sont utilisés pour assigner des valeurs à des variables. Les opérateurs d'incrémentation (++) et de décrémentation (--) sont utilisés pour augmenter ou diminuer la valeur d'une variable de manière itérative.
                        2.3 Structures de contrôle
                        - Instructions conditionnelles (if-else)
                        Les instructions conditionnelles if, else if et else sont utilisées pour exécuter des blocs de code en fonction de conditions spécifiées. Par exemple :
                        java

                        Copy code
                        int x = 10;
                        if (x > 0) {
                            System.out.println("x est positif");
                        } else {
                            System.out.println("x est négatif ou nul");
                        }
                        - Boucles (for, while, do-while)
                        Les boucles permettent d'exécuter un bloc de code de manière répétée jusqu'à ce qu'une condition spécifiée soit remplie. Java prend en charge les boucles for, while et do-while.
                        - Utilisation de break et continue
                        Les instructions break et continue sont utilisées pour contrôler le flux d'exécution dans une boucle. break est utilisé pour sortir de la boucle, tandis que continue est utilisé pour passer à l'itération suivante de la boucle.
                        2.4 Tableaux
                        - Déclaration et initialisation des tableaux
                        Un tableau en Java est une structure de données permettant de stocker plusieurs valeurs de même type. Les tableaux sont déclarés en spécifiant le type des éléments du tableau et sa taille. Par exemple :
                        java

                        Copy code
                        int[] tableau = new int[5]; // Déclaration et initialisation d'un tableau d'entiers de taille 5
                        - Accès aux éléments d'un tableau
                        Les éléments d'un tableau sont accessibles en utilisant leur indice, qui commence généralement à zéro pour le premier élément et se termine à la taille du tableau moins un.
                        - Parcours de tableaux avec des boucles
                        Les boucles sont souvent utilisées pour parcourir les éléments d'un tableau. Vous pouvez utiliser une boucle for pour itérer à travers chaque élément du tableau et effectuer des opérations sur eux.""", javaIntroduction, 2);

        Course javaPOO = new Course("Programmation orientée objet en Java", java, 2);

        Lesson javaPOOIntro = new Lesson("Class, Héritage et polymorphisme",
                """
                        Dans ce chapitre, nous plongerons dans les principes fondamentaux de la programmation orientée objet (POO) en Java. Nous explorerons les concepts de base tels que les classes, les objets, les méthodes, ainsi que des concepts avancés tels que l'encapsulation, l'héritage, le polymorphisme, les interfaces et les classes abstraites.

                        3.1 Introduction à la POO
                        - Concepts fondamentaux (classes, objets, méthodes, etc.)
                        La programmation orientée objet est un paradigme de programmation qui organise le code autour d'objets et de leurs interactions. Les principaux concepts incluent les classes (modèles pour les objets), les objets (instances de classes), les méthodes (fonctions associées aux objets), les attributs (propriétés des objets), etc.
                        - Encapsulation, héritage et polymorphisme
                        L'encapsulation est le mécanisme qui consiste à regrouper les données (attributs) et les méthodes qui les manipulent dans une seule unité appelée classe. L'héritage permet à une classe (sous-classe) d'hériter des propriétés et des comportements d'une autre classe (superclasse). Le polymorphisme permet à un objet de se comporter de différentes manières en fonction du contexte
                        3.2 Définition et utilisation de classes
                        - Déclaration de classes
                        Une classe en Java est déclarée à l'aide du mot-clé class, suivi du nom de la classe et du corps de la classe, qui contient les attributs et les méthodes de la classe.
                        - Création d'objets
                        Pour créer un objet à partir d'une classe, vous utilisez le mot-clé new, suivi du nom de la classe et de parenthèses (si la classe a un constructeur par défaut). Par exemple :
                        java

                        Copy code
                        MaClasse monObjet = new MaClasse();
                        - Méthodes et attributs de classe
                        Les méthodes sont des fonctions associées à une classe qui permettent de manipuler les données de la classe. Les attributs sont des variables qui décrivent l'état d'un objet. Ils sont déclarés à l'intérieur de la classe et peuvent être accessibles à partir de différentes méthodes de la classe.
                        3.3 Héritage et polymorphisme
                        - Création de hiérarchies de classes
                        L'héritage en Java permet de créer des relations "est-un" entre les classes. Une classe enfant (sous-classe) peut hériter des propriétés et des méthodes d'une classe parent (superclasse), ce qui favorise la réutilisation du code et la modélisation de relations entre les objets.
                        - Redéfinition de méthodes
                        Une classe enfant peut redéfinir (ou substituer) une méthode héritée de sa classe parente pour modifier ou étendre son comportement.
                        - Utilisation du polymorphisme
                        Le polymorphisme permet à un objet d'être traité comme un objet de son type parent, ce qui permet une plus grande flexibilité dans la conception logicielle. Par exemple, une méthode peut prendre un paramètre de type parent et accepter des objets de types différents, mais dérivant du type parent.
                        3.4 Interfaces et classes abstraites
                        - Définition et utilisation d'interfaces
                        Une interface en Java définit un ensemble de méthodes sans fournir d'implémentation. Les classes peuvent implémenter des interfaces, ce qui garantit qu'elles fournissent une implémentation pour toutes les méthodes définies par l'interface.
                        - Utilisation de classes abstraites
                        Une classe abstraite est une classe qui ne peut pas être instanciée directement et qui peut contenir des méthodes abstraites (méthodes sans implémentation) ainsi que des méthodes concrètes (avec implémentation). Les classes dérivées doivent fournir une implémentation pour les méthodes abstraites de la classe abstraite.""", javaPOO, 1);

        Course javaAlgo = new Course("Algorithmie en Java", java, 3);

        Lesson javaAlgoPartie = new Lesson("Première partie", """
                Dans ce chapitre, nous explorerons les bases de l'algorithmie en Java, en commençant par les concepts fondamentaux tels que les algorithmes et leur complexité, puis en plongeant dans les structures de données fondamentales, les algorithmes classiques et enfin en abordant la résolution de problèmes algorithmiques.
                4.1 Introduction à l'algorithmie
                - Notions de base (algorithmes, complexité, etc.)
                L'algorithmie est l'art de concevoir des algorithmes efficaces pour résoudre des problèmes. Un algorithme est une séquence d'étapes finies permettant de résoudre un problème donné. Nous examinerons également la complexité des algorithmes, qui mesure la quantité de ressources (temps et espace) requise pour les exécuter.
                - Importance de l'efficacité algorithmique
                L'efficacité algorithmique est cruciale pour résoudre les problèmes de manière efficace et optimale. Nous discuterons de l'importance de choisir des algorithmes efficaces pour optimiser les performances de nos programmes.
                4.2 Structures de données fondamentales
                - Listes, piles, files
                Nous explorerons les structures de données fondamentales telles que les listes (comme les tableaux et les listes chaînées), les piles et les files, en examinant leurs caractéristiques et en discutant de leurs applications.
                - Arbres et graphes
                Les arbres et les graphes sont des structures de données plus complexes utilisées pour représenter des relations hiérarchiques et non hiérarchiques entre des éléments. Nous examinerons les différents types d'arbres et de graphes et discuterons de leurs propriétés.
                4.3 Algorithmes classiques
                - Recherche séquentielle et binaire
                Nous aborderons les algorithmes de recherche séquentielle et binaire, utilisés pour rechercher des éléments dans une liste ordonnée ou non ordonnée.
                - Tri (tri bulle, tri par insertion, tri rapide)
                Les algorithmes de tri sont largement utilisés pour organiser des données dans un ordre spécifique. Nous étudierons des algorithmes de tri classiques tels que le tri bulle, le tri par insertion et le tri rapide.
                - Parcours de graphes (parcours en profondeur, parcours en largeur)
                Les graphes peuvent être parcourus en profondeur (DFS) ou en largeur (BFS). Nous examinerons ces algorithmes de parcours et discuterons de leurs applications.
                4.4 Résolution de problèmes algorithmiques
                - Méthodologie de résolution de problèmes
                Nous présenterons une méthodologie générale pour résoudre des problèmes algorithmiques, en mettant l'accent sur la compréhension du problème, la conception d'une solution, l'implémentation de l'algorithme et l'analyse de sa complexité.
                - Exemples de problèmes courants et leurs solutions algorithmiques en Java
                Nous explorerons plusieurs exemples de problèmes courants tels que la recherche d'un élément maximal dans une liste, la recherche de chemins dans un graphe, la recherche d'un élément dans un arbre, etc., et discuterons de leurs solutions algorithmiques en Java.""", javaAlgo, 1);

        Quiz IntroductionJava = new Quiz(" Evaluation d'Introduction Java",
                """
                        Ce quiz porte sur l’introduction de Java et vise à évaluer vos connaissances sur ce langage de programmation largement utilisé. Il comprend quatre questions à choix unique, chacune couvrant un aspect différent de Java, de son développement à son utilité.
                        """, javaIntroductionJava);

        Question Question1 = new Question("Qu'est-ce que Java ?", IntroductionJava);

        Answer Answer1 = new Answer("Un langage de programmation développé par Microsoft",
                false, Question1);
        Answer Answer2 = new Answer("Un langage de programmation portable, orienté objet et sécurisé",
                true, Question1);
        Answer Answer3 = new Answer("Un langage de script utilisé pour le développement web",
                false, Question1);
        Answer Answer4 = new Answer("Un langage de programmation obsolète",
                false, Question1);


        Question Question2 = new Question("Qui est souvent crédité comme le père fondateur de Java ?", IntroductionJava);

        Answer Answer2_1 = new Answer("Bill Gates",
                false, Question2);
        Answer Answer2_2 = new Answer("James Gosling",
                true, Question2);
        Answer Answer2_3 = new Answer("Linus Torvalds",
                false, Question2);
        Answer Answer2_4 = new Answer("Larry Page",
                false, Question2);


        Question Question3 = new Question("Pourquoi Java est-il largement utilisé ?", IntroductionJava);

        Answer Answer3_1 = new Answer("Pour sa difficulté d'apprentissage",
                false, Question3);
        Answer Answer3_2 = new Answer("Pour sa portabilité, son caractère orienté objet et sa sécurité",
                true, Question3);
        Answer Answer3_3 = new Answer("Pour sa limitation aux applications mobiles uniquement",
                false, Question3);
        Answer Answer3_4 = new Answer("Pour sa compatibilité uniquement avec les systèmes Windows",
                false, Question3);


        Question Question4 = new Question("Quel est l'acronyme de JDK ?", IntroductionJava);

        Answer Answer4_1 = new Answer("Java Developer Kit",
                true, Question4);
        Answer Answer4_2 = new Answer("JavaScript Developer Kit",
                false, Question4);
        Answer Answer4_3 = new Answer("Java Distribution Kit",
                false, Question4);
        Answer Answer4_4 = new Answer("Java Development Kit",
                true, Question4);


        Quiz IntroductionBase = new Quiz("Evaluation de Base Java ",
                """
                        Ce quiz vise à évaluer vos connaissances sur les bases du langage de programmation Java. Les questions abordent différents aspects, tels que les types de données primitifs, la déclaration de variables, la portée des variables, et les opérateurs utilisés en Java.

                        """, javaIntroductionBase);

        Question Question5 = new Question("Quels sont les types de données primitifs pris en charge par Java ?", IntroductionBase);

        Answer Answer5_1 = new Answer("String, float, char",
                false, Question1);
        Answer Answer5_2 = new Answer("int, double, boolean",
                true, Question1);
        Answer Answer5_3 = new Answer("array, long, byte",
                false, Question1);
        Answer Answer5_4 = new Answer("void, short, object",
                false, Question1);


        Question Question6 = new Question("Comment déclarez-vous une variable en Java ?", IntroductionBase);

        Answer Answer6_1 = new Answer("var variableName;",
                false, Question2);
        Answer Answer6_2 = new Answer("variableType variableName;",
                false, Question2);
        Answer Answer6_3 = new Answer("type variableName;",
                true, Question2);
        Answer Answer6_4 = new Answer("name type;",
                false, Question2);


        Question Question7 = new Question("Quelle est la portée d'une variable locale en Java ?", IntroductionBase);

        Answer Answer7_1 = new Answer("Elle est accessible dans tout le programme.",
                false, Question3);
        Answer Answer7_2 = new Answer("Elle est limitée à un bloc de code spécifique.",
                true, Question3);
        Answer Answer7_3 = new Answer("Elle peut être utilisée dans différentes classes.",
                false, Question3);
        Answer Answer7_4 = new Answer("Elle est accessible uniquement dans les méthodes.",
                false, Question3);


        Question Question8 = new Question("Quel est l'opérateur utilisé pour l'incrémentation en Java ?", IntroductionBase);

        Answer Answer8_1 = new Answer("++",
                true, Question4);
        Answer Answer8_2 = new Answer("+=",
                false, Question4);
        Answer Answer8_3 = new Answer("--",
                false, Question4);
        Answer Answer8_4 = new Answer("-=",
                false, Question4);

        Quiz POOIntro = new Quiz(" Evaluation Class, Héritage et polymorphisme",
                """
                        Ce quiz vise à évaluer vos connaissances sur la programmation orientée objet (POO), un paradigme de programmation largement utilisé. Les questions abordent les principaux concepts de la POO et leur application en Java.
                        """, javaPOOIntro);

        Question Question9 = new Question("Quel est le principal objectif de la programmation orientée objet ?", POOIntro);

        Answer Answer9_1 = new Answer("Organiser le code autour d'objets et de leurs interactions",
                true, Question1);
        Answer Answer9_2 = new Answer("Écrire des instructions linéaires pour résoudre des problèmes",
                false, Question1);
        Answer Answer9_3 = new Answer("Créer des fonctions autonomes pour chaque tâche",
                false, Question1);
        Answer Answer9_4 = new Answer("Développer des programmes sans utiliser de classes",
                false, Question1);


        Question Question10 = new Question("Qu'est-ce que l'encapsulation en programmation orientée objet ?", POOIntro);

        Answer Answer10_1 = new Answer("Le regroupement des données et des méthodes dans une seule unité appelée classe",
                true, Question2);
        Answer Answer10_2 = new Answer("L'utilisation de nombreuses classes pour organiser le code",
                false, Question2);
        Answer Answer10_3 = new Answer("La conversion des méthodes en attributs dans une classe",
                false, Question2);
        Answer Answer10_4 = new Answer("La division des méthodes en plusieurs parties pour une meilleure lisibilité",
                false, Question2);


        Question Question11 = new Question("Quel est l'objectif principal de l'héritage en Java ?", POOIntro);

        Answer Answer11_1 = new Answer("Créer des relations \"est-un\" entre les classes",
                true, Question3);
        Answer Answer11_2 = new Answer("Créer des relations \"a une\" entre les classes",
                false, Question3);
        Answer Answer11_3 = new Answer("Éliminer l'utilisation des classes parentes",
                false, Question3);
        Answer Answer11_4 = new Answer("Limiter l'accès aux méthodes et attributs de la classe parente",
                false, Question3);


        Question Question12 = new Question("Qu'est-ce que le polymorphisme permet à un objet de faire ?", POOIntro);

        Answer Answer12_1 = new Answer("Se comporter de différentes manières en fonction du contexte",
                true, Question4);
        Answer Answer13_2 = new Answer("Réduire la complexité du code en Java",
                false, Question4);
        Answer Answer13_3 = new Answer("Empêcher l'utilisation d'objets de types différents",
                false, Question4);
        Answer Answer13_4 = new Answer("Créer des relations \"est-un\" entre les objets",
                false, Question4);


        Quiz AlgoPartie = new Quiz(" Evaluation Algorithmique",
                """
                        Ce quiz vise à évaluer votre compréhension des bases de l'algorithmie en Java. Il comprend une série de questions, couvrant différents aspects de l'algorithmie et de ses applications en programmation. Les questions abordent les concepts fondamentaux tels que la définition d'un algorithme, la mesure de la complexité des algorithmes, l'importance de l'efficacité algorithmique en programmation, ainsi que les structures de données fondamentales et les méthodes de résolution de problèmes algorithmiques.
                        """, javaAlgoPartie);

        Question Question14 = new Question("Qu'est-ce que l'algorithmie ?", AlgoPartie);

        Answer Answer14_1 = new Answer("L'art de concevoir des algorithmes efficaces pour résoudre des problèmes",
                true, Question1);
        Answer Answer14_2 = new Answer("L'art de concevoir des algorithmes inefficaces",
                false, Question1);
        Answer Answer14_3 = new Answer("L'art de créer des programmes sans utiliser d'algorithmes",
                false, Question1);
        Answer Answer14_4 = new Answer("L'art de programmer en Java",
                false, Question1);


        Question Question15 = new Question("Pourquoi l'efficacité algorithmique est-elle importante en programmation ?", AlgoPartie);

        Answer Answer15_1 = new Answer("Pour résoudre les problèmes de manière efficace et optimale",
                true, Question2);
        Answer Answer15_2 = new Answer("Pour rendre les programmes plus lents",
                false, Question2);
        Answer Answer15_3 = new Answer("Pour rendre les programmes plus complexes",
                false, Question2);
        Answer Answer15_4 = new Answer("Pour compliquer la conception des algorithmes",
                false, Question2);


        Question Question16 = new Question("Quelles sont des structures de données fondamentales en Java ?", AlgoPartie);

        Answer Answer16_1 = new Answer("Listes, piles, files",
                true, Question3);
        Answer Answer16_2 = new Answer("Tables de hachage, arbres binaires, files",
                false, Question3);
        Answer Answer16_3 = new Answer("Graphes, tables de hachage, arbres binaires",
                false, Question3);
        Answer Answer16_4 = new Answer("Arbres binaires, tables de hachage, listes",
                false, Question3);


        Question Question17 = new Question("Quel est l'objectif des algorithmes de tri ?", AlgoPartie);

        Answer Answer17_1 = new Answer("Organiser les données dans un ordre spécifique",
                true, Question4);
        Answer Answer17_2 = new Answer("Désorganiser les données",
                false, Question4);
        Answer Answer17_3 = new Answer("Créer des données aléatoires",
                false, Question4);
        Answer Answer17_4 = new Answer("Supprimer les données de la mémoire",
                false, Question4);


        trainingRepository.save(java);
        courseRepository.saveAll(List.of(javaAlgo, javaIntroduction, javaPOO));
        lessonRepository.saveAll(List.of(javaAlgoPartie, javaIntroductionJava, javaIntroductionBase, javaPOOIntro));
        quizRepository.saveAll(List.of(AlgoPartie, IntroductionJava, IntroductionBase, POOIntro));
    }

    public void loadPythonData(TrainingRepository trainingRepository,
                               QuizRepository quizRepository) {
        Training python = new Training("Débuter en Python", "Bienvenue dans ce cours passionnant qui vous emmènera dans un voyage captivant à travers le monde de la programmation en Python et de l'algorithmique. Que vous soyez un débutant absolu ou un programmeur expérimenté cherchant à approfondir vos connaissances, ce cours est conçu pour vous offrir une expérience d'apprentissage enrichissante et gratifiante.");

        Course pythonLesBases = new Course("Les bases du python et de l’algorithmie", python, 1);

        Lesson pythonLesBasesIntro = new Lesson("Introduction à Python",
                """
                        Section 1.1 : Qu'est-ce que Python ?
                        Introduction à Python
                        Python est un langage de programmation interprété, orienté objet et polyvalent, créé par Guido van Rossum et publié pour la première fois en 1991. Il est devenu l'un des langages les plus populaires dans le domaine de la programmation en raison de sa syntaxe simple et lisible, de sa polyvalence et de sa vaste communauté de développeurs.
                        Avantages et caractéristiques de Python
                        Clarté syntaxique : La syntaxe de Python est simple et facile à lire, ce qui en fait un excellent choix pour les débutants et les programmeurs expérimentés.
                        Polyvalence : Python peut être utilisé pour une variété de tâches, allant du développement web à l'analyse de données, en passant par l'automatisation de tâches et le développement d'applications.
                        Grande bibliothèque standard : Python est livré avec une vaste bibliothèque standard qui offre des modules et des fonctions pour effectuer une multitude de tâches courantes sans avoir besoin d'installer des bibliothèques tierces.
                        Communauté active : Python bénéficie d'une grande communauté de développeurs qui contribuent à son développement, fournissent un soutien technique et partagent des ressources utiles.
                        Section 1.2 : Installation de Python
                        Téléchargement et installation de Python
                        Pour commencer à utiliser Python, vous devez d'abord télécharger et installer l'interpréteur Python à partir du site officiel de Python (https://www.python.org/). Le processus d'installation est généralement simple et bien documenté pour les différents systèmes d'exploitation.
                        Environnements de développement (IDE)
                        Une fois Python installé, vous pouvez choisir un environnement de développement intégré (IDE) pour écrire, déboguer et exécuter vos programmes Python. Certains des IDE populaires pour Python incluent PyCharm, VS Code, et Jupyter Notebook, chacun offrant ses propres fonctionnalités et avantages.
                        Section 1.3 : Votre premier programme Python
                        Écrire et exécuter un programme Python
                        Pour écrire un programme Python, vous pouvez utiliser n'importe quel éditeur de texte, comme Notepad ou Sublime Text. Commencez par écrire votre code Python en utilisant la syntaxe que vous avez apprise, puis enregistrez-le avec l'extension ".py". Ensuite, vous pouvez exécuter votre programme en ouvrant une fenêtre de terminal ou de ligne de commande, en accédant au répertoire où se trouve votre fichier Python, puis en tapant "python nom_du_fichier.py".
                        Utilisation de l'interpréteur Python interactif
                        Python est également livré avec un interpréteur interactif qui vous permet d'exécuter du code Python ligne par ligne et d'obtenir des résultats immédiats. Pour accéder à l'interpréteur Python interactif, ouvrez une fenêtre de terminal ou de ligne de commande et tapez simplement "python".
                        Section 1.4 : Les types de données de base en Python
                        Nombres (entiers, décimaux)
                        En Python, vous pouvez manipuler différents types de nombres, y compris les entiers (integers) et les nombres décimaux (floats). Vous pouvez effectuer des opérations mathématiques de base comme l'addition, la soustraction, la multiplication et la division sur ces nombres.
                        Chaînes de caractères
                        Les chaînes de caractères (strings) sont utilisées pour représenter du texte en Python. Vous pouvez créer des chaînes en entourant le texte de guillemets simples ('') ou doubles (""). Python offre de nombreuses opérations de manipulation de chaînes, telles que la concaténation, la découpe (slicing) et la recherche de sous-chaînes.
                        Listes
                        Les listes sont des collections ordonnées d'éléments en Python. Elles peuvent contenir n'importe quel type de données, et vous pouvez ajouter, supprimer ou modifier des éléments dans une liste. Les listes sont définies en entourant les éléments de crochets [] et en les séparant par des virgules.
                        Tuples
                        Les tuples sont similaires aux listes, mais ils sont immuables, ce qui signifie que vous ne pouvez pas modifier leur contenu une fois qu'ils sont créés. Les tuples sont définis en entourant les éléments de parenthèses () et en les séparant par des virgules.
                        Dictionnaires
                        Les dictionnaires sont des collections associatives de paires clé-valeur en Python. Chaque élément d'un dictionnaire est constitué d'une clé et de sa valeur correspondante, et vous pouvez utiliser la clé pour accéder à la valeur associée. Les dictionnaires sont définis en entourant les paires clé-valeur de crochets {} et en les séparant par des virgules.

                        Ce sont les bases essentielles pour démarrer votre voyage dans le monde de Python. Chaque section vous fournit les connaissances nécessaires pour comprendre les concepts fondamentaux de Python et pour écrire vos premiers programmes. Continuez à explorer et à pratiquer pour approfondir votre compréhension et votre maîtrise de Python
                        """, pythonLesBases, 1);

        Lesson pythonLesBasesStructuresFonctions = new Lesson("Structures de contrôle et fonctions",
                """
                        Dans ce chapitre, nous explorerons les structures de contrôle et les fonctions en Python, qui sont des éléments fondamentaux pour contrôler le flux d'exécution de votre programme et pour organiser votre code de manière efficace.
                        Section 2.1 : Structures de contrôle conditionnelles
                        Les structures de contrôle conditionnelles permettent d'exécuter des blocs de code en fonction de conditions spécifiques.
                        Instruction if
                        L'instruction if permet d'exécuter un bloc de code si une condition est vraie.
                        Instruction else
                        L'instruction else permet d'exécuter un bloc de code si la condition de l'instruction if est fausse.
                        Instruction elif
                        L'instruction elif (abréviation de "else if") permet de vérifier plusieurs conditions en cascade.
                        Utilisation de l'opérateur ternaire
                        L'opérateur ternaire est une expression conditionnelle compacte qui permet d'écrire des instructions if en une seule ligne.
                        Section 2.2 : Boucles en Python
                        Les boucles permettent d'exécuter un bloc de code plusieurs fois, en fonction de certaines conditions.
                        Boucle while
                        La boucle while permet d'exécuter un bloc de code tant qu'une condition est vraie.
                        Boucle for
                        La boucle for permet d'itérer sur des éléments d'une séquence (comme une liste ou une chaîne de caractères).
                        Instructions break et continue
                        L'instruction break permet de sortir prématurément d'une boucle, tandis que l'instruction continue permet de passer à l'itération suivante de la boucle.
                        Section 2.3 : Fonctions en Python
                        Les fonctions permettent de regrouper des instructions afin de les réutiliser plus tard dans votre code.
                        Définition de fonctions
                        Vous pouvez définir vos propres fonctions en utilisant le mot-clé def, suivi du nom de la fonction et de ses paramètres.
                        Arguments et paramètres de fonction
                        Les fonctions peuvent accepter des arguments, qui sont des valeurs fournies lors de l'appel de la fonction, et des paramètres, qui sont des variables utilisées pour manipuler ces valeurs à l'intérieur de la fonction.
                        Valeurs de retour
                        Les fonctions peuvent renvoyer des valeurs à l'aide du mot-clé return, permettant ainsi de retourner des résultats calculés à partir des arguments fournis.
                        Portée des variables
                        La portée d'une variable détermine où elle peut être utilisée dans votre code. Les variables définies à l'intérieur d'une fonction ont une portée locale, tandis que celles définies à l'extérieur d'une fonction ont une portée globale.
                        """, pythonLesBases, 2);

        Lesson pythonLesBasesStructuresDeDonnees = new Lesson("Structures de données avancées",
                """
                        Dans ce chapitre, nous explorerons des structures de données avancées en Python qui vous permettront de gérer et manipuler des collections de données de manière efficace. Nous aborderons les listes avec des opérations avancées, les tuples et ensembles, ainsi que les dictionnaires avancés.
                        Section 3.1 : Listes et opérations avancées
                        Les listes sont des structures de données polyvalentes qui peuvent contenir une collection ordonnée d'éléments. Dans cette section, nous découvrirons des techniques avancées pour manipuler les listes.
                        Tranches (slicing)
                        Les tranches permettent de sélectionner une partie spécifique d'une liste en utilisant une syntaxe concise. Cela vous permet d'accéder à des sous-listes en spécifiant un début, une fin et un pas.
                        Compréhensions de liste
                        Les compréhensions de liste sont une manière concise et élégante de créer des listes en Python. Elles vous permettent de générer des listes en une seule ligne en utilisant une syntaxe compacte et expressive.
                        Fonctions intégrées pour les listes
                        Python offre de nombreuses fonctions intégrées pour manipuler les listes de manière efficace. Nous explorerons des fonctions telles que len(), append(), extend(), insert(), remove(), pop(), index(), count(), et sort() pour effectuer diverses opérations sur les listes
                        Section 3.2 : Tuples et ensembles
                        Les tuples et ensembles sont des structures de données similaires aux listes mais avec des fonctionnalités spécifiques. Dans cette section, nous découvrirons leurs caractéristiques et leurs opérations.
                        Utilisation et manipulation des tuples
                        Les tuples sont des collections immuables d'éléments, ce qui signifie qu'ils ne peuvent pas être modifiés une fois créés. Nous verrons comment créer et manipuler des tuples, ainsi que les avantages de les utiliser dans certaines situations.
                        Utilisation des ensembles et opérations sur les ensembles
                        Les ensembles sont des collections non ordonnées d'éléments uniques. Nous aborderons les opérations fondamentales sur les ensembles telles que l'union, l'intersection, la différence, et la vérification de l'appartenance.
                        Section 3.3 : Dictionnaires avancés
                        Les dictionnaires sont des structures de données clé-valeur puissantes qui permettent de stocker et d'accéder à des données de manière efficace. Dans cette section, nous explorerons des techniques avancées pour manipuler les dictionnaires.
                        Manipulation avancée des dictionnaires
                        Nous verrons comment ajouter, supprimer et mettre à jour des éléments dans un dictionnaire, ainsi que des techniques pour itérer sur les clés et les valeurs d'un dictionnaire.
                        Compréhensions de dictionnaire
                        Les compréhensions de dictionnaire sont une manière élégante de créer des dictionnaires en une seule ligne en utilisant une syntaxe concise et expressive.
                        Méthodes de dictionnaire
                        Python offre de nombreuses méthodes intégrées pour manipuler les dictionnaires. Nous explorerons des méthodes telles que keys(), values(), items(), get(), pop(), popitem(), clear(), et update() pour effectuer diverses opérations sur les dictionnaires""", pythonLesBases, 3);
        Lesson pythonLesBasesAlgoPython = new Lesson("Introduction à l'algorithmique en Python",
                """
                        Dans ce chapitre, nous plongerons dans le monde de l'algorithmique en utilisant Python comme outil principal. Nous aborderons les notions de base en algorithmique, les algorithmes de tri et les techniques de recherche de données, ainsi que la manipulation de fichiers en Python.
                        Section 4.1 : Notions de base en algorithmique
                        Qu'est-ce qu'un algorithme ?
                        Un algorithme est une séquence d'instructions finie et non ambiguë conçue pour résoudre un problème spécifique. Nous examinerons la structure d'un algorithme, son rôle dans la résolution de problèmes, et les différentes caractéristiques qui définissent un bon algorithme.
                        Complexité algorithmique
                        La complexité algorithmique est l'analyse de la performance d'un algorithme en termes de temps d'exécution et de ressources requises. Nous étudierons les notations Big O, Omega, et Theta pour évaluer la complexité temporelle et spatiale des algorithmes.
                        Section 4.2 : Algorithmes de tri
                        Tri à bulles
                        Le tri à bulles est un algorithme de tri simple mais inefficace qui parcourt répétitivement la liste, compare les éléments adjacents et les échange si nécessaire. Nous verrons comment implémenter cet algorithme en Python et discuterons de sa complexité.
                        Tri par sélection
                        Le tri par sélection est un autre algorithme de tri simple qui divise la liste en deux sous-listes : une sous-liste triée et une sous-liste non triée. Nous étudierons son fonctionnement et son implémentation en Python.
                        Tri par insertion
                        Le tri par insertion consiste à construire une liste triée un élément à la fois en insérant chaque nouvel élément à sa place appropriée. Nous explorerons son fonctionnement et discuterons de sa complexité temporelle.
                        Section 4.3 : Recherche et manipulation de données
                        Recherche séquentielle
                        La recherche séquentielle est une méthode simple et intuitive pour rechercher un élément dans une liste en parcourant séquentiellement chaque élément. Nous verrons comment mettre en œuvre cet algorithme en Python.
                        Recherche binaire
                        La recherche binaire est une méthode de recherche plus efficace qui divise répétitivement la liste en deux parties et recherche l'élément cible dans la moitié appropriée. Nous étudierons son fonctionnement et sa mise en œuvre en Python.
                        Manipulation de fichiers en Python
                        Nous explorerons les différentes techniques de manipulation de fichiers en Python, y compris l'ouverture, la lecture, l'écriture et la fermeture de fichiers. Nous verrons comment manipuler des fichiers texte et des fichiers binaires, ainsi que des opérations avancées telles que la gestion des exceptions et le déplacement du curseur.""", pythonLesBases, 4);
        Course pythonProjets = new Course("Projet d’entrainement", python, 2);

        Lesson pythonProjetsConsignes = new Lesson("Création de 2 applications",
                """
                        Dans cette section, nous vous proposons deux projets d'entraînement pour mettre en pratique les concepts appris dans les sections précédentes. Le premier projet consiste à créer un gestionnaire de tâches simple, tandis que le deuxième projet porte sur le développement d'un jeu interactif.
                        Section 5.1 : Création d'un gestionnaire de tâches simple
                        Dans ce projet, vous allez créer un gestionnaire de tâches simple en utilisant Python. Vous utiliserez des structures de données pour stocker les tâches et implémenterez les fonctionnalités d'ajout, de suppression et de modification de tâches.
                        Objectifs du projet :
                        Utiliser des structures de données pour stocker les tâches.
                        Implémenter les fonctionnalités d'ajout, de suppression et de modification de tâches.
                        Section 5.2 : Développement d'un jeu simple
                        Dans ce projet, vous développerez un jeu simple en utilisant Python. Vous utiliserez des boucles et des conditions pour créer un jeu interactif et gérerez les entrées utilisateur pour permettre aux joueurs d'interagir avec le jeu.
                        Objectifs du projet :
                        Utiliser des boucles et des conditions pour créer un jeu interactif.
                        Gérer les entrées utilisateur pour permettre aux joueurs d'interagir avec le jeu.
                        Ces projets vous donneront l'occasion de mettre en pratique vos connaissances en Python et d'améliorer vos compétences de programmation en travaillant sur des applications concrètes. N'hésitez pas à être créatif et à personnaliser ces projets selon vos préférences et votre niveau de compétence en programmation. Bonne chance !""", pythonProjets, 1);

        Quiz LesBasesIntro = new Quiz(" Evaluation à l'Introduction à Python",
                """
                        Le quiz portant sur l'introduction à Python couvre les principes fondamentaux et les caractéristiques essentielles de ce langage de programmation populaire :
                        Qu'est-ce que Python ?
                        a) Un langage de programmation compilé
                        b) Un langage de programmation orienté objet
                        c) Un langage de programmation interprété
                        d) Un langage de programmation basé sur C++

                        Quel avantage Python offre-t-il en termes de clarté syntaxique ?
                        a) Sa syntaxe est complexe et difficile à lire
                        b) Sa syntaxe est simple et facile à lire
                        c) Il n'a pas de syntaxe définie
                        d) Sa syntaxe est similaire à celle du langage machine

                        Pourquoi Python est-il considéré comme polyvalent ?
                        a) Il est limité à des tâches spécifiques telles que le développement web
                        b) Il ne peut être utilisé que pour l'analyse de données
                        c) Il peut être utilisé pour une variété de tâches telles que le développement web, l'analyse de données, etc.
                        d) Il ne peut pas être utilisé pour l'automatisation de tâches

                        Quel est l'un des avantages de l'utilisation de l'interpréteur Python interactif ?
                        a) Il nécessite l'installation d'un IDE
                        b) Il fournit des résultats immédiats
                        c) Il ne prend pas en charge l'exécution de code Python
                        d) Il est plus lent que l'exécution de code Python à partir de fichiers

                        """, pythonLesBasesIntro);

        Quiz LesBasesStructuresFonctions = new Quiz(" Evaluation au Structures de contrôle et fonctions",
                """
                        Ce quiz porte sur les structures de contrôle et les fonctions en Python, des concepts fondamentaux pour contrôler le flux d'exécution des programmes et organiser le code de manière efficace. Les questions abordent différents aspects de ces sujets, allant des structures de contrôle conditionnelles telles que les instructions if, else, et elif, aux boucles while et for, ainsi qu'aux instructions break et continue. De plus, le quiz couvre également les bases de la définition de fonctions en Python, y compris la déclaration de fonctions, les arguments et les paramètres, les valeurs de retour, ainsi que la portée des variables. Les questions sont formulées pour évaluer la compréhension des apprenants sur ces concepts et leur capacité à les appliquer dans des scénarios pratiques.
                        Quelle est la fonction principale des structures de contrôle conditionnelles en Python ?
                        a) Contrôler le flux d'exécution du programme
                        b) Définir des fonctions
                        c) Manipuler des chaînes de caractères
                        d) Créer des listes

                        Que permet l'instruction if en Python ?
                        a) Exécuter un bloc de code si une condition est vraie
                        b) Exécuter un bloc de code si une condition est fausse
                        c) Vérifier plusieurs conditions en cascade
                        d) Répéter un bloc de code plusieurs fois

                        Quelle est la fonction de l'instruction elif en Python ?
                        a) Exécuter un bloc de code si une condition est vraie
                        b) Exécuter un bloc de code si une condition est fausse
                        c) Vérifier plusieurs conditions en cascade
                        d) Exécuter un bloc de code tant qu'une condition est vraie

                        Quelle boucle en Python permet d'itérer sur des éléments d'une séquence comme une liste ou une chaîne de caractères ?
                        a) Boucle if
                        b) Boucle while
                        c) Boucle for
                        d) Boucle continue

                        """, pythonLesBasesStructuresFonctions);

        Quiz LesBasesStructuresDeDonnees = new Quiz(" Evaluation de Structures de données avancées",
                """
                        Les questions du quiz sont conçues pour évaluer la compréhension des concepts clés abordés dans ce chapitre. Les apprenants seront testés sur leur connaissance des opérations avancées sur les listes, telles que les tranches (slicing) et les compréhensions de liste, ainsi que sur leur capacité à comprendre l'utilisation et la manipulation des tuples, des ensembles et des dictionnaires.
                        Quelle est la principale caractéristique des tranches (slicing) en Python ?
                        a) Elles permettent de sélectionner une partie spécifique d'une liste en utilisant une syntaxe concise.
                        b) Elles permettent de concaténer deux listes en une seule.
                        c) Elles permettent de trier une liste dans l'ordre décroissant.
                        d) Elles permettent de supprimer des éléments d'une liste.

                        Quelle est l'utilité des compréhensions de liste en Python ?
                        a) Elles permettent de créer des fonctions.
                        b) Elles permettent de générer des listes en une seule ligne de code.
                        c) Elles permettent de modifier les valeurs d'une liste.
                        d) Elles permettent de définir des conditions dans une liste.

                        Quelle fonction intégrée en Python est utilisée pour ajouter un élément à la fin d'une liste ?
                        a) add()
                        b) append()
                        c) insert()
                        d) extend()

                        Quelle structure de données en Python est immuable, ce qui signifie qu'elle ne peut pas être modifiée une fois créée ?
                        a) Listes
                        b) Tuples
                        c) Ensembles
                        d) Dictionnaires

                        """, pythonLesBasesStructuresDeDonnees);

        Quiz LesBasesAlgoPython = new Quiz("Introduction à l'algorithmique en Python",
                """
                        Ce quiz permettra aux apprenants de consolider leur compréhension des algorithmes de base, des structures de données et des opérations fondamentales de manipulation de données en Python, ce qui est essentiel pour développer des compétences en programmation et en résolution de problèmes.
                        Qu'est-ce qu'un algorithme ?
                        a) Une séquence d'instructions infinie
                        b) Une séquence d'instructions finie et non ambiguë conçue pour résoudre un problème spécifique
                        c) Une liste de fonctions pour compiler un programme
                        d) Une structure de données pour stocker des valeurs

                        Que mesure la complexité algorithmique ?
                        a) La longueur de l'algorithme
                        b) Le nombre de fonctions utilisées
                        c) La quantité de ressources (temps et espace) requise pour l'exécution
                        d) Le nombre de variables déclarées

                        Quel est l'un des algorithmes de tri abordés dans la section 4.2 ?
                        a) Tri rapide
                        b) Tri fusion
                        c) Tri linéaire
                        d) Tri inversé

                        Comment fonctionne la recherche séquentielle ?
                        a) Elle divise la liste en deux parties et recherche l'élément cible dans la moitié appropriée
                        b) Elle parcourt séquentiellement chaque élément de la liste jusqu'à trouver l'élément cible
                        c) Elle trie la liste avant de rechercher l'élément cible
                        d) Elle effectue une recherche binaire dans la liste

                        """, pythonLesBasesAlgoPython);

        trainingRepository.save(python);
        quizRepository.saveAll(List.of(LesBasesAlgoPython,
                LesBasesIntro,
                LesBasesStructuresDeDonnees,
                LesBasesStructuresFonctions));
    }

    public void loadSQLData(TrainingRepository trainingRepository,
                            CourseRepository courseRepository,
                            LessonRepository lessonRepository) {
        Training sql = new Training("SQL et base de données", "SQL (Structured Query Language) est un langage de programmation utilisé pour gérer les bases de données relationnelles. Il permet de stocker, manipuler et interroger les données de manière efficace. Les bases de données servent à organiser des informations structurées de manière logique, facilitant ainsi leur accès, leur manipulation et leur gestion.");

        Course sqlIntroduction = new Course("Introduction à SQL", sql, 1);

        Lesson sqlIntroductionDefinition = new Lesson("1. Qu'est-ce que SQL ?",
                """
                        1. Qu'est-ce que SQL ?
                        Définition de SQL : SQL (Structured Query Language) est un langage de programmation utilisé pour gérer les bases de données relationnelles. Il permet de manipuler les données en effectuant des opérations telles que l'insertion, la mise à jour, la suppression et la récupération des données.
                        Utilité et importance dans la gestion des bases de données : SQL est essentiel dans la gestion des bases de données car il fournit une interface standardisée et puissante pour interagir avec les données. Il permet aux développeurs et aux administrateurs de bases de données de créer, de maintenir et de récupérer des données de manière efficace, tout en assurant l'intégrité et la cohérence des données.


                        2. Historique de SQL
                        Origines et évolution : SQL a été initialement développé par IBM dans les années 1970 pour interagir avec leurs bases de données relationnelles. Au fil du temps, il a évolué avec les contributions d'autres entreprises telles qu'Oracle, Microsoft et ANSI, devenant ainsi un langage de base de données standard largement utilisé.
                        Standardisation : La standardisation de SQL a été entreprise par l'ANSI (American National Standards Institute) et l'ISO (International Organization for Standardization). Cela a permis de définir un ensemble de règles et de normes pour assurer la compatibilité et la portabilité entre les différents systèmes de gestion de bases de données relationnelles.
                        3. Types de bases de données supportées
                        Bases de données relationnelles : Les bases de données relationnelles utilisent le modèle relationnel pour organiser les données sous forme de tables, avec des relations entre les différentes tables. SQL est principalement utilisé pour interagir avec ce type de bases de données, offrant une grande flexibilité et une efficacité dans la manipulation des données.
                        Bases de données NoSQL : Contrairement aux bases de données relationnelles, les bases de données NoSQL sont conçues pour gérer des données non relationnelles ou semi-structurées. Bien que SQL ne soit pas toujours utilisé dans les bases de données NoSQL, certains systèmes NoSQL offrent des extensions SQL ou des interfaces compatibles avec SQL pour permettre une certaine compatibilité avec les requêtes SQL traditionnelles.""", sqlIntroduction, 1);
        Lesson sqlIntroductionHistorique = new Lesson("Historique de SQL",
                """
                        Origines et évolution : SQL a été initialement développé par IBM dans les années 1970 pour interagir avec leurs bases de données relationnelles. Au fil du temps, il a évolué avec les contributions d'autres entreprises telles qu'Oracle, Microsoft et ANSI, devenant ainsi un langage de base de données standard largement utilisé.
                        Standardisation : La standardisation de SQL a été entreprise par l'ANSI (American National Standards Institute) et l'ISO (International Organization for Standardization). Cela a permis de définir un ensemble de règles et de normes pour assurer la compatibilité et la portabilité entre les différents systèmes de gestion de bases de données relationnelles.
                        """, sqlIntroduction, 2);

        Lesson sqlIntroductionBaseSupporte = new Lesson("Types de bases de données supportées",
                "Bases de données relationnelles : Les bases de données relationnelles utilisent le modèle relationnel pour organiser les données sous forme de tables, avec des relations entre les différentes tables. SQL est principalement utilisé pour interagir avec ce type de bases de données, offrant une grande flexibilité et une efficacité dans la manipulation des données." +
                        "Bases de données NoSQL : Contrairement aux bases de données relationnelles, les bases de données NoSQL sont conçues pour gérer des données non relationnelles ou semi-structurées. Bien que SQL ne soit pas toujours utilisé dans les bases de données NoSQL, certains systèmes NoSQL offrent des extensions SQL ou des interfaces compatibles avec SQL pour permettre une certaine compatibilité avec les requêtes SQL traditionnelles.", sqlIntroduction, 3);

        Course sqlBaseRelationnel = new Course("Structure de base d'une base de données relationnelle", sql, 2);

        Lesson sqlBaseRelationnelTables = new Lesson("Tables",
                "Définition et rôle : Les tables sont des structures fondamentales dans une base de données relationnelle. Elles permettent d'organiser les données de manière logique en lignes et en colonnes. Chaque table représente une entité ou un concept, avec chaque ligne représentant une instance de cette entité et chaque colonne représentant un attribut de cette entité." +
                        "Exemples de tables : Par exemple, dans une base de données d'une librairie, vous pourriez avoir une table \"Livres\" contenant des informations telles que le titre, l'auteur et l'année de publication des livres, ainsi qu'une table \"Auteurs\" contenant des informations sur les auteurs comme leur nom et leur nationalité", sqlBaseRelationnel, 1);

        Lesson sqlBasesRelationnelColonneAndType = new Lesson("Colonnes et types de données",
                "Types de données les plus courants : Les bases de données relationnelles supportent différents types de données pour représenter différentes valeurs. Parmi les types de données les plus courants, on trouve INTEGER (entiers), VARCHAR (chaînes de caractères de longueur variable), DATE (dates), etc." +
                        "Contraintes : Les contraintes définissent les règles d'intégrité et de validité des données dans une table. Parmi les contraintes les plus courantes, on trouve NOT NULL (la valeur ne peut pas être nulle), PRIMARY KEY (clé primaire, unique pour chaque ligne) et FOREIGN KEY (clé étrangère, liée à une clé primaire dans une autre table).", sqlBaseRelationnel, 2);

        Course sqlRequetes = new Course("Requêtes SQL", sql, 3);

        Lesson sqlRequestesSelect = new Lesson("Introduction aux requêtes SELECT",
                "Syntaxe de base : L'instruction SELECT est utilisée pour récupérer des données à partir d'une base de données. Sa syntaxe de base est \"SELECT colonnes FROM table\", où vous spécifiez les colonnes que vous souhaitez récupérer et la table à partir de laquelle vous souhaitez les récupérer." +
                        "Utilisation de l'opérateur SELECT : L'opérateur SELECT peut être utilisé pour récupérer toutes les colonnes d'une table en utilisant \"*\", ou des colonnes spécifiques en les spécifiant séparément.", sqlRequetes, 1);

        Lesson sqlRequestesWhere = new Lesson("Clauses WHERE et ORDER BY",
                """
                        Filtrer les résultats : La clause WHERE est utilisée pour filtrer les résultats d'une requête en spécifiant des conditions qui doivent être remplies par les lignes retournées. Par exemple, "WHERE age > 18" sélectionnerait uniquement les personnes dont l'âge est supérieur à 18.
                        Ordonner les résultats : La clause ORDER BY est utilisée pour trier les résultats d'une requête en fonction des valeurs d'une ou plusieurs colonnes. Par exemple, "ORDER BY nom ASC" trierait les résultats par ordre alphabétique croissant du nom.
                        """, sqlRequetes, 2);

        Lesson sqlRequestesGroupHaving = new Lesson("Clauses GROUP BY et HAVING",
                "Agréger les données : La clause GROUP BY est utilisée pour regrouper les résultats d'une requête en fonction des valeurs d'une ou plusieurs colonnes. Par exemple, \"GROUP BY ville\" regrouperait les résultats par ville." +
                        "Filtrer les données agrégées : La clause HAVING est utilisée pour filtrer les résultats d'une requête groupée en spécifiant des conditions qui doivent être remplies par les groupes retournés. Par exemple, \"HAVING COUNT(*) > 5\" sélectionnerait uniquement les groupes ayant plus de 5 éléments.", sqlRequetes, 3);

        Course sqlManipulationDonnees = new Course("Manipulation de données", sql, 4);

        Lesson sqlManipulationDonneesInsert = new Lesson("Insertion de données",
                """
                        Syntaxe de l'instruction INSERT : L'instruction INSERT est utilisée pour ajouter de nouvelles lignes à une table. Sa syntaxe de base est "INSERT INTO table (colonne1, colonne2, ...) VALUES (valeur1, valeur2, ...)", où vous spécifiez les colonnes dans lesquelles vous voulez insérer des données et les valeurs correspondantes que vous souhaitez insérer.
                        Exemples d'insertion de données :
                        sql

                        Copy code
                        INSERT INTO Employes (nom, age, salaire) VALUES ('Jean Dupont', 30, 50000);
                        INSERT INTO Produits (nom_produit, prix) VALUES ('Téléphone', 799.99);
                        """, sqlManipulationDonnees, 1);

        Lesson sqlManipulationDonneesMaj = new Lesson("Mise à jour de données",
                """
                        Syntaxe de l'instruction UPDATE : L'instruction UPDATE est utilisée pour modifier des données existantes dans une table. Sa syntaxe est "UPDATE table SET colonne1 = valeur1, colonne2 = valeur2, ... WHERE condition", où vous spécifiez les colonnes à mettre à jour et les nouvelles valeurs, ainsi que les lignes à modifier en fonction d'une condition.
                        Exemples de mise à jour de données :
                        sql

                        Copy code
                        UPDATE Employes SET salaire = 55000 WHERE nom = 'Jean Dupont';
                        UPDATE Produits SET prix = 899.99 WHERE nom_produit = 'Téléphone';""", sqlManipulationDonnees, 2);

        Lesson sqlManipulationDonneesSuppression = new Lesson("Suppression de données",
                """
                        Syntaxe de l'instruction DELETE : L'instruction DELETE est utilisée pour supprimer des lignes d'une table. Sa syntaxe est "DELETE FROM table WHERE condition", où vous spécifiez les lignes à supprimer en fonction d'une condition.
                        Précautions à prendre lors de la suppression de données : Il est important de prendre des précautions lors de la suppression de données pour éviter la perte accidentelle de données importantes. Par exemple, vous pouvez utiliser la clause WHERE pour limiter la suppression à des lignes spécifiques, ou effectuer une sauvegarde des données avant de les supprimer.
                        Exemple de suppression de données :
                        sql

                        Copy code
                        DELETE FROM Employes WHERE nom = 'Jean Dupont';
                        Il est recommandé de tester attentivement les instructions INSERT, UPDATE et DELETE avant de les exécuter sur des données de production pour éviter toute altération non désirée des données.
                        """, sqlManipulationDonnees, 3);

        Course sqlJointures = new Course("Jointures", sql, 5);

        Lesson sqlJointuresIntro = new Lesson("Introduction aux jointures",
                """
                        Concept de jointure : Une jointure est une opération permettant de combiner les données de deux ou plusieurs tables en fonction d'une condition de correspondance entre les colonnes de ces tables. Les jointures permettent d'enrichir les résultats des requêtes en associant des données provenant de différentes sources.
                        Types de jointures :
                        INNER JOIN : Retourne uniquement les lignes pour lesquelles il existe une correspondance dans les deux tables.
                        LEFT JOIN : Retourne toutes les lignes de la table de gauche (première table spécifiée) et les lignes correspondantes de la table de droite.
                        RIGHT JOIN : Retourne toutes les lignes de la table de droite et les lignes correspondantes de la table de gauche.
                        FULL JOIN : Retourne toutes les lignes lorsque qu'il y a une correspondance dans l'une des tables.
                        """, sqlJointures, 1);

        Lesson sqlJointureSyntaxes = new Lesson("Syntaxe des jointures",
                """
                        Utilisation des clauses ON et USING : Les jointures sont généralement spécifiées à l'aide de clauses telles que ON ou USING, qui indiquent les conditions de correspondance entre les colonnes des tables à joindre.
                        Exemples de jointures :
                        sql

                        Copy code
                        -- INNER JOIN
                        SELECT *
                        FROM Employes
                        INNER JOIN Departements ON Employes.departement_id = Departements.departement_id;

                        -- LEFT JOIN
                        SELECT *
                        FROM Clients
                        LEFT JOIN Commandes ON Clients.client_id = Commandes.client_id;

                        -- RIGHT JOIN
                        SELECT *
                        FROM Commandes
                        RIGHT JOIN Clients ON Commandes.client_id = Clients.client_id;

                        -- FULL JOIN
                        SELECT *
                        FROM Employes
                        FULL JOIN Salaires ON Employes.employe_id = Salaires.employe_id;
                        Les jointures permettent de combiner efficacement des données provenant de différentes tables, ce qui est essentiel pour analyser et extraire des informations complexes à partir de bases de données relationnelles.
                        """, sqlJointures, 2);

        Course sqlSousRequestes = new Course("Sous-requêtes et expressions de table", sql, 6);

        Lesson sqlSousRequestesSsR = new Lesson("Sous-requêtes",
                """
                        Définition et utilité : Une sous-requête est une requête SQL imbriquée à l'intérieur d'une autre requête SQL. Elle permet d'utiliser les résultats d'une requête comme condition ou valeur dans une autre requête. Les sous-requêtes offrent une flexibilité supplémentaire dans la récupération et la manipulation des données, permettant des opérations plus complexes.
                        Utilisation dans les clauses WHERE, SELECT et FROM : Les sous-requêtes peuvent être utilisées dans différentes parties d'une requête SQL. Dans la clause WHERE, elles permettent de filtrer les résultats en fonction des résultats d'une autre requête. Dans la clause SELECT, elles peuvent être utilisées pour récupérer une valeur à afficher dans les résultats. Dans la clause FROM, elles peuvent être utilisées pour récupérer des données à partir d'une sous-requête.
                        """, sqlSousRequestes, 1);

        Lesson sqlSousRequestesExpression = new Lesson("Expressions de table",
                """
                        Définition et syntaxe : Une expression de table est une construction SQL qui permet de créer temporairement une table dans une requête SELECT. Elle est définie à l'aide de la clause WITH, suivie du nom de la table temporaire et de sa définition. Les expressions de table sont utiles pour simplifier les requêtes complexes en les divisant en parties plus gérables.
                        Utilisation pour créer des tables temporaires : Les expressions de table sont utilisées pour définir des tables temporaires qui peuvent être référencées dans la requête principale. Elles permettent de décomposer une requête complexe en étapes plus simples et plus lisibles, ce qui facilite la compréhension et la maintenance du code SQL. Exemple d'utilisation des sous-requêtes :
                        sql

                        Copy code
                        -- Utilisation dans la clause WHERE
                        SELECT nom
                        FROM Employes
                        WHERE departement_id IN (SELECT departement_id FROM Departements WHERE nom = 'Ventes');

                        -- Utilisation dans la clause SELECT
                        SELECT nom, (SELECT COUNT(*) FROM Commandes WHERE Commandes.client_id = Clients.client_id) AS nombre_commandes
                        FROM Clients;

                        -- Utilisation dans la clause FROM
                        SELECT *
                        FROM (SELECT departement_id, COUNT(*) AS nombre_employes FROM Employes GROUP BY departement_id) AS statistiques;

                        Exemple d'utilisation des expressions de table :
                        sql

                        Copy code
                        WITH Ventas AS (
                            SELECT client_id
                            FROM Commandes
                            WHERE date_commande >= '2022-01-01'
                        )
                        SELECT *
                        FROM Clients
                        WHERE client_id IN (SELECT client_id FROM Ventas);
                        Les sous-requêtes et les expressions de table sont des outils puissants en SQL qui permettent d'effectuer des opérations complexes et d'améliorer la lisibilité et la maintenabilité du code SQL.

                        """, sqlSousRequestes, 2);

        Course sqlFonction = new Course("Fonctions SQL", sql, 7);

        Lesson sqlFonctionAgregation = new Lesson("Fonctions d'agrégation",
                """
                        SUM : Calcule la somme des valeurs d'une colonne.
                        AVG : Calcule la moyenne des valeurs d'une colonne.
                        COUNT : Compte le nombre de lignes dans un résultat.
                        MAX : Retourne la valeur maximale d'une colonne.
                        MIN : Retourne la valeur minimale d'une colonne
                        Utilisation dans les requêtes : Les fonctions d'agrégation sont utilisées dans les requêtes pour effectuer des calculs sur un ensemble de données. Elles sont souvent utilisées en conjonction avec la clause GROUP BY pour regrouper les données avant d'appliquer la fonction d'agrégation.
                        """, sqlFonction, 1);

        Lesson sqlFonctionCaracteres = new Lesson("Fonctions de chaînes de caractères",
                """
                        CONCAT : Concatène deux ou plusieurs chaînes de caractères.
                        SUBSTRING : Extrait une sous-chaîne à partir d'une chaîne donnée.
                        UPPER : Convertit une chaîne en majuscules.
                        LOWER : Convertit une chaîne en minuscules
                        Manipulation de chaînes de caractères : Les fonctions de chaînes de caractères sont utilisées pour manipuler et formater les données textuelles dans les requêtes SQL. Elles peuvent être utilisées pour combiner des chaînes, extraire des sous-chaînes, ou changer la casse des caractères.
                        """, sqlFonction, 2);

        Lesson sqlFonctionDate = new Lesson("Fonctions de date et d'heure",
                """
                        Exemples de fonctions :
                        NOW : Retourne la date et l'heure actuelles.
                        DATE_FORMAT : Formate une date selon un modèle spécifié.
                        DATE_ADD : Ajoute une période à une date donnée.
                        DATE_DIFF : Calcule la différence entre deux dates   - Manipulation de données temporelles
                        Manipulation de données temporelles : Les fonctions de date et d'heure sont utilisées pour manipuler les données temporelles dans les requêtes SQL. Elles permettent de réaliser des calculs sur les dates, de les formater selon différents formats, ou encore de calculer des écarts de temps entre deux dates.
                        Exemples d'utilisation :
                        -- Fonctions d'agrégation
                        SELECT SUM(quantite) AS total_articles, AVG(prix) AS prix_moyen FROM Commandes;
                        SELECT COUNT(*) AS nombre_commandes FROM Commandes;
                        SELECT MAX(date_commande) AS derniere_commande FROM Commandes;

                        -- Fonctions de chaînes de caractères
                        SELECT CONCAT(prenom, ' ', nom) AS nom_complet FROM Employes;
                        SELECT UPPER(nom) AS nom_majuscule FROM Clients;
                        SELECT SUBSTRING(description, 1, 50) AS description_courte FROM Produits;

                        -- Fonctions de date et d'heure
                        SELECT NOW() AS date_actuelle;
                        SELECT DATE_FORMAT(date_commande, '%Y-%m-%d') AS date_format FROM Commandes;
                        SELECT DATE_ADD(date_livraison, INTERVAL 7 DAY) AS date_livraison_prevue FROM Commandes;
                        SELECT DATE_DIFF(date_livraison, date_commande) AS delai_livraison FROM Commandes;
                        Les fonctions SQL offrent une variété d'outils pour manipuler et analyser les données dans les bases de données relationnelles, ce qui permet d'effectuer des opérations complexes et d'extraire des informations pertinentes.
                        """, sqlFonction, 3);

        Course sqlSecurity = new Course("Sécurité et bonnes pratiques", sql, 8);

        Lesson sqlSecurityInRequestes = new Lesson("Sécurité dans les requêtes SQL",
                """
                        Injection SQL : L'injection SQL est une attaque où des données non vérifiées sont insérées dans une requête SQL, permettant à un attaquant d'exécuter du code SQL malveillant. Cela peut entraîner des dommages graves tels que la suppression de données, la divulgation d'informations sensibles ou la compromission du système.
                        Prévention des attaques : Pour prévenir les attaques par injection SQL, il est essentiel d'utiliser des requêtes préparées ou des paramètres pour passer des valeurs aux requêtes SQL plutôt que de concaténer directement les valeurs dans la requête. Cela aide à séparer les données des instructions SQL, réduisant ainsi le risque d'injection SQL.
                        """, sqlSecurity, 1);

        Lesson sqlSecurityBonnePratique = new Lesson("Bonnes pratiques de développement",
                """
                        Nommer les objets de base de données de manière significative : Il est important de nommer les tables, les colonnes et les autres objets de base de données de manière significative et cohérente. Cela facilite la compréhension du schéma de la base de données et des requêtes SQL, ainsi que la maintenance du code.
                        Éviter les requêtes complexes et redondantes : Les requêtes SQL complexes et redondantes peuvent être difficiles à comprendre, à maintenir et à optimiser. Il est recommandé d'éviter les requêtes complexes en les divisant en étapes plus simples et en utilisant des techniques telles que les sous-requêtes et les expressions de table. De plus, l'utilisation de vues peut aider à réduire la redondance des requêtes en encapsulant des requêtes fréquemment utilisées dans des objets de base de données réutilisables.
                        En suivant ces bonnes pratiques de sécurité et de développement, vous pouvez réduire les risques d'attaques par injection SQL et améliorer la qualité, la performance et la maintenabilité de vos applications basées sur des bases de données relationnelles.
                        """, sqlSecurity, 1);
        trainingRepository.save(sql);
        courseRepository.saveAll(List.of(sqlFonction,
                sqlJointures,
                sqlSecurity,
                sqlIntroduction,
                sqlRequetes,
                sqlBaseRelationnel,
                sqlManipulationDonnees,
                sqlSousRequestes));
        lessonRepository.saveAll(List.of(sqlFonctionDate,
                sqlFonctionAgregation,
                sqlFonctionCaracteres,
                sqlJointuresIntro,
                sqlJointureSyntaxes,
                sqlSecurityBonnePratique,
                sqlSecurityInRequestes,
                sqlIntroductionHistorique,
                sqlIntroductionBaseSupporte,
                sqlIntroductionDefinition,
                sqlRequestesGroupHaving,
                sqlRequestesWhere,
                sqlRequestesSelect,
                sqlBaseRelationnelTables,
                sqlBasesRelationnelColonneAndType,
                sqlManipulationDonneesInsert,
                sqlManipulationDonneesMaj,
                sqlManipulationDonneesSuppression,
                sqlSousRequestesSsR,
                sqlSousRequestesExpression));
    }

    public void loadSymfonyData(TrainingRepository trainingRepository,
                                QuizRepository quizRepository) {
        Training symfony = new Training("Framework Symfony ", "Symfony est un framework PHP open-source et hautement extensible, conçu pour simplifier et accélérer le développement d'applications web. Il offre une architecture solide basée sur le modèle MVC, une gestion avancée des routes, des vues avec Twig, une sécurité intégrée et une gestion de base de données avec Doctrine. Ce cours présente les bases essentielles de Symfony, allant de l'installation initiale à la création de formulaires, en passant par l'accès aux données et la mise en place de tests.");

        Course symfonyIntroduction = new Course("Introduction à Symfony", symfony, 1);

        Lesson symfonyIntroductionDef = new Lesson("Qu'est-ce que Symfony ?",
                """
                        Symfony est un framework PHP open-source, créé par SensioLabs, qui offre une structure robuste 
                        et flexible pour le développement d'applications web. Initialement lancé en 2005, Symfony a 
                        évolué pour devenir l'un des frameworks PHP les plus populaires et les plus respectés de l'industrie.

                        Introduction à Symfony et son histoire : Symfony a été conçu pour simplifier le processus de 
                        développement web en fournissant des outils et des conventions bien définis. Son développement communautaire actif garantit des mises à jour régulières et un support continu.
                        Avantages de l'utilisation de Symfony : Symfony offre de nombreux avantages, notamment une architecture modulaire, une grande flexibilité, une excellente documentation et une vaste communauté de développeurs. Il favorise également les bonnes pratiques de développement telles que la réutilisabilité du code, la testabilité et la sécurité.
                        Composants principaux de Symfony : Symfony est composé de nombreux composants indépendants qui 
                        peuvent être utilisés séparément ou ensemble pour répondre aux besoins spécifiques d'une application. 
                        Ces composants incluent des éléments pour la gestion des requêtes HTTP, la validation des données, 
                        l'accès aux bases de données, la sécurité, la gestion des formulaires, etc""", symfonyIntroduction, 1);
        Lesson symfonyIntroductionInstallation = new Lesson("Installation de Symfony",
                """
                        Prérequis système : Avant d'installer Symfony, il est important de s'assurer que votre système satisfait aux prérequis nécessaires, tels que PHP, Composer, et d'autres dépendances spécifiques au projet.
                        Installation via Composer : Symfony peut être installé facilement à l'aide de Composer, un gestionnaire de dépendances pour PHP. Composer simplifie le processus d'installation en téléchargeant automatiquement toutes les dépendances requises pour votre projet.
                        Configuration initiale de Symfony : Une fois installé, Symfony nécessite une configuration initiale pour démarrer un nouveau projet. Cela inclut la création de la structure de répertoires de base, la configuration des paramètres du projet et la définition des routes initiales.
                        En comprenant ces concepts fondamentaux et en suivant les étapes d'installation, vous serez prêt à commencer à utiliser Symfony pour le développement de vos applications web.""", symfonyIntroduction, 2);


        Course symfonyStructureApp = new Course("Structure d'une Application Symfony", symfony, 2);

        Lesson symfonyStructureAppMVC = new Lesson("Architecture MVC (Modèle-Vue-Contrôleur)",
                """
                        Dans ce premier sous-chapitre, nous explorerons en détail le concept d'architecture Modèle-Vue-Contrôleur (MVC) et son rôle fondamental dans le développement d'applications Symfony. Nous examinerons comment Symfony implémente ce modèle et comment il sépare les préoccupations de la logique métier, de la présentation et de la gestion des requêtes.

                        Explication du modèle MVC et son utilisation dans Symfony: Nous plongerons dans les trois composants du modèle - Modèle, Vue et Contrôleur - en expliquant comment chacun interagit dans le cadre de Symfony.
                        Rôles et responsabilités des Modèles, Vues et Contrôleurs: Nous détaillerons les responsabilités de chaque composant et comment ils coopèrent pour créer des applications Symfony robustes et modulaires.""", symfonyStructureApp, 1);

        Lesson symfonyStructureAppStructures = new Lesson("Structure des répertoires",
                """
                        Ce sous-chapitre se concentrera sur la disposition des fichiers et des répertoires dans un projet Symfony. Une compréhension claire de la structure du projet est essentielle pour une organisation efficace du code et une collaboration fluide entre les membres de l'équipe de développement.

                        Organisation des fichiers et répertoires dans un projet Symfony: Nous examinerons la hiérarchie des répertoires et expliquerons leur usage approprié pour différents types de fichiers et de ressources.
                        Explication des répertoires principaux (src, config, templates, etc.): Nous passerons en revue les répertoires principaux tels que src pour le code source, config pour les configurations, templates pour les vues, etc. Nous discuterons également des conventions de nommage et des bonnes pratiques associées à chaque répertoire.
                        Ce chapitre jettera les bases essentielles pour comprendre la structure d'une application Symfony, ce qui sera crucial pour le développement ultérieur de fonctionnalités et la maintenance du projet.""", symfonyStructureApp, 2);

        Course symfonyRoutesControllers = new Course("Routes et Contrôleurs", symfony, 3);

        Lesson symfonyRoutesControllersRoutes = new Lesson("Configuration des routes",
                """
                        La configuration des routes est essentielle pour diriger les requêtes HTTP vers les bonnes actions de contrôleurs dans une application Symfony. Dans ce sous-chapitre, nous explorerons en détail la manière de configurer les routes.

                        Utilisation du fichier routes.yaml pour définir les routes: Nous découvrirons comment définir des routes dans le fichier routes.yaml, en spécifiant les chemins, les méthodes HTTP et les contrôleurs associés.
                        Syntaxe des routes et paramètres dynamiques: Nous examinerons la syntaxe des routes Symfony, y compris la définition de paramètres dynamiques pour capturer des valeurs variables dans les URL.""", symfonyRoutesControllers, 1);

        Lesson symfonyRoutesControllersController = new Lesson("Création de Contrôleurs",
                """
                        Les contrôleurs agissent comme des gestionnaires de requêtes HTTP et contiennent la logique métier de l'application. Dans cette section, nous aborderons la création et la gestion des contrôleurs dans Symfony.

                        Création de contrôleurs pour gérer les requêtes HTTP: Nous apprendrons à créer des contrôleurs dans Symfony, en définissant des actions pour répondre à des routes spécifiques.
                        Injection de dépendances dans les contrôleurs: Nous discuterons de l'importance de l'injection de dépendances dans les contrôleurs et de la manière de l'utiliser pour accéder à d'autres services et composants de l'application.
                        Comprendre la configuration des routes et la création des contrôleurs est fondamental pour développer des applications Symfony efficaces et bien structurées. Ce chapitre fournira une base solide pour la manipulation des requêtes HTTP et la mise en œuvre de la logique métier dans votre application Symfony.""", symfonyRoutesControllers, 2);


        Course symfonyVuesTwig = new Course("Les Vues et Twig", symfony, 4);

        Lesson symfonyVuesTwigIntro = new Lesson("Introduction à Twig",
                """
                        Twig est un moteur de template flexible et puissant utilisé dans Symfony pour générer des vues HTML de manière efficace et sécurisée. Dans ce sous-chapitre, nous explorerons les bases de Twig.

                        Qu'est-ce que Twig ?: Nous fournirons une introduction à Twig, expliquant son rôle dans Symfony et ses avantages par rapport à d'autres moteurs de template.
                        Syntaxe de base de Twig : variables, boucles, conditions, etc.: Nous passerons en revue la syntaxe de base de Twig, y compris l'utilisation de variables, les structures de contrôle telles que les boucles et les conditions, ainsi que d'autres fonctionnalités utiles.""", symfonyVuesTwig, 1);

        Lesson symfonyVuesTwigIntegration = new Lesson("Intégration des Vues dans Symfony",
                """
                        Dans cette section, nous aborderons l'intégration de Twig dans Symfony et son utilisation pour générer des vues dynamiques.

                        Utilisation de Twig pour générer des vues dans Symfony: Nous découvrirons comment utiliser Twig dans Symfony pour créer des fichiers de vue, en mettant l'accent sur la création de modèles réutilisables et la manipulation de données.
                        Partage de données entre le contrôleur et la vue: Nous discuterons des différentes méthodes pour partager des données entre le contrôleur et la vue, telles que l'utilisation de variables et de fonctions spécifiques de Twig.
                        La maîtrise de Twig est essentielle pour développer des interfaces utilisateur attrayantes et dynamiques dans Symfony. Ce chapitre vous donnera les connaissances nécessaires pour utiliser efficacement Twig dans vos projets Symfony et créer des vues interactives et conviviales.""", symfonyVuesTwig, 2);


        Course symfonyFormulaire = new Course("Gestion des Formulaires", symfony, 5);

        Lesson symfonyFormulaireCreate = new Lesson("Création de Formulaires",
                """
                        Les formulaires sont des éléments essentiels dans de nombreuses applications web, et Symfony offre une manière efficace de les gérer. Dans ce sous-chapitre, nous aborderons la création et la validation des formulaires.

                        Utilisation des classes de formulaire Symfony: Nous explorerons comment créer des formulaires en utilisant les classes de formulaire Symfony, en définissant les champs et les options associées.
                        Validation des données de formulaire: Nous discuterons de la validation des données de formulaire, en utilisant les validateurs intégrés de Symfony pour garantir que les données soumises sont conformes aux règles définies.""", symfonyFormulaire, 1);

        Lesson symfonyFormulaireTraitement = new Lesson("Traitement des données de formulaire",
                """
                        Une fois que les utilisateurs soumettent des données via un formulaire, il est important de les traiter correctement du côté du serveur. Ce sous-chapitre se concentrera sur le traitement des données de formulaire et la gestion des erreurs éventuelles.

                        Récupération et manipulation des données soumises: Nous verrons comment récupérer les données soumises à partir du formulaire dans Symfony et les manipuler selon les besoins de l'application.
                        Gestion des erreurs de formulaire: Nous aborderons la gestion des erreurs de formulaire, en affichant des messages d'erreur appropriés et en aidant les utilisateurs à corriger les données incorrectes.
                        La gestion des formulaires est une partie cruciale du développement d'applications web, et Symfony fournit des outils puissants pour simplifier ce processus. Ce chapitre vous donnera les compétences nécessaires pour créer et valider des formulaires dans vos projets Symfony de manière efficace et sécurisée""", symfonyFormulaire, 2);


        Course symfonyDoctrine = new Course("Accès aux Données avec Doctrine", symfony, 6);

        Lesson symfonyDoctrineORM = new Lesson("Introduction à Doctrine ORM",
                """
                        Doctrine est un ORM (Object-Relational Mapping) puissant et flexible utilisé dans Symfony pour simplifier l'accès aux données et la gestion de la base de données. Dans cette première section, nous explorerons les bases de Doctrine.

                        Qu'est-ce que Doctrine ?: Nous fournirons une introduction à Doctrine, expliquant son rôle dans Symfony et les avantages de l'utilisation d'un ORM.
                        Configuration de Doctrine dans Symfony: Nous aborderons les étapes nécessaires pour configurer Doctrine dans un projet Symfony, y compris la connexion à la base de données et la configuration des entités.""", symfonyDoctrine, 1);

        Lesson symfonyDoctrineModel = new Lesson("Création de Modèles avec Doctrine Entities",
                """
                        Les entités Doctrine représentent les données de votre application et définissent la structure de votre base de données. Dans cette section, nous verrons comment créer et utiliser des entités dans Symfony.

                        Création d'entités pour représenter les données dans la base de données: Nous expliquerons comment définir des entités Doctrine pour représenter les différentes tables de la base de données, en spécifiant les propriétés et les relations.
                        Annotations Doctrine pour définir les relations et les contraintes: Nous utiliserons les annotations Doctrine pour définir des relations entre les entités, ainsi que des contraintes telles que les clés primaires et étrangères.""", symfonyDoctrine, 2);

        Lesson symfonyDoctrineRepositories = new Lesson("Utilisation des Repositories",
                """
                        Les repositories sont utilisés pour accéder et manipuler les entités dans Symfony. Cette dernière section du chapitre se concentrera sur l'utilisation des repositories pour effectuer des opérations de lecture et d'écriture sur les données.

                        Accès aux données à l'aide des repositories: Nous verrons comment utiliser les méthodes des repositories pour effectuer des opérations de base telles que la recherche, l'ajout, la modification et la suppression d'entités.
                        Requêtes personnalisées avec DQL (Doctrine Query Language): Nous introduirons le langage de requête Doctrine (DQL) et expliquerons comment l'utiliser pour écrire des requêtes SQL personnalisées lorsque les méthodes des repositories ne sont pas suffisantes.
                        La compréhension de Doctrine est cruciale pour le développement d'applications web robustes et bien structurées avec Symfony. Ce chapitre vous fournira les connaissances nécessaires pour gérer efficacement l'accès aux données dans vos projets Symfony en utilisant Doctrine ORM.""", symfonyDoctrine, 3);

        Course symfonySecurity = new Course("Sécurité dans Symfony", symfony, 7);

        Lesson symfonySecurityConfig = new Lesson("Configuration de la Sécurité",
                """
                        La sécurité est une préoccupation majeure pour toute application web, et Symfony propose des fonctionnalités puissantes pour protéger vos applications contre les menaces potentielles. Dans cette section, nous aborderons la configuration de la sécurité dans Symfony.

                        Mise en place de la sécurité dans Symfony: Nous explorerons les différentes options de sécurité disponibles dans Symfony, telles que la protection contre les attaques CSRF (Cross-Site Request Forgery), la protection XSS (Cross-Site Scripting), etc.
                        Utilisation du pare-feu de sécurité Symfony: Nous verrons comment configurer un pare-feu de sécurité dans Symfony pour contrôler l'accès aux différentes parties de votre application en fonction des rôles et des permissions des utilisateurs.
                        """, symfonySecurity, 1);

        Lesson symfonySecurityAuth = new Lesson("Authentification et Autorisation",
                """
                        L'authentification et l'autorisation sont des aspects critiques de la sécurité d'une application web. Dans cette section, nous approfondirons ces concepts dans le contexte de Symfony.

                        Gestion de l'authentification des utilisateurs: Nous aborderons les différentes méthodes d'authentification des utilisateurs dans Symfony, telles que l'authentification par formulaire, l'authentification par jeton, etc.
                        Contrôle des accès aux ressources en fonction des rôles et des permissions: Nous discuterons de la manière de mettre en œuvre l'autorisation dans Symfony en définissant des rôles et des permissions pour contrôler l'accès aux différentes fonctionnalités et ressources de l'application.
                        La sécurité est un aspect fondamental de tout projet web, et une bonne compréhension des fonctionnalités de sécurité de Symfony est essentielle pour garantir la protection de votre application contre les menaces. Ce chapitre vous fournira les connaissances nécessaires pour sécuriser efficacement vos projets Symfony.""", symfonySecurity, 2);

        Course symfonyTests = new Course("Tests dans Symfony", symfony, 8);

        Lesson symfonyTestsTypes = new Lesson("Types de tests",
                """
                        Les tests jouent un rôle crucial dans le développement d'applications robustes et fiables. Dans cette section, nous examinerons les différents types de tests disponibles dans Symfony.

                        Unit tests, tests fonctionnels et tests d'intégration: Nous expliquerons les différences entre ces types de tests et quand les utiliser dans le contexte de Symfony.
                        Utilisation des outils de tests Symfony (PHPUnit, BrowserKit, etc.): Nous présenterons les outils de tests disponibles dans Symfony, tels que PHPUnit pour les tests unitaires et BrowserKit pour les tests fonctionnels, ainsi que leur intégration dans les projets Symfony.""", symfonyTests, 1);

        Lesson symfonyTestsEcriture = new Lesson("Écriture de Tests",
                """
                        Dans cette section, nous aborderons les meilleures pratiques pour l'écriture de tests dans Symfony et nous verrons comment écrire des tests pour différents composants de l'application.

                        Création de tests pour les contrôleurs, les services et les entités: Nous explorerons les stratégies pour écrire des tests pour les différents composants de l'application Symfony, y compris les contrôleurs, les services et les entités.
                        Bonnes pratiques pour l'écriture de tests dans Symfony: Nous partagerons des bonnes pratiques pour l'écriture de tests dans Symfony, y compris l'utilisation de fixtures, l'isolation des tests et l'intégration continue.
                        Les tests sont essentiels pour garantir la qualité et la stabilité d'une application Symfony. Ce chapitre vous fournira les connaissances nécessaires pour écrire et exécuter des tests efficaces dans vos projets Symfony, vous permettant ainsi de développer des applications fiables et de haute qualité.""", symfonyTests, 2);

        Quiz IntroductionDefInstall = new Quiz("Evaluation sur la definition et l'installation de Symfony",
                """
                        Ce quiz vise à évaluer vos connaissances sur Symfony, un framework PHP open-source largement utilisé pour le développement d'applications web. Il couvre les aspects essentiels de Symfony, y compris sa définition, ses avantages, ses composants principaux et son processus d'installation.
                        Les questions sont formulées avec des réponses à choix unique pour tester votre compréhension des concepts clés de Symfony
                        
                        Qu'est-ce que Symfony ?
                        a) Un framework JavaScript pour le développement d'applications web
                        b) Un langage de programmation utilisé pour les applications mobiles
                        c) Un framework PHP open-source pour le développement d'applications web
                        d) Un système de gestion de base de données relationnelle
                        
                        Quels sont les composants principaux de Symfony ?
                        a) Des composants pour la création de graphiques en 3D
                        b) Des éléments pour la gestion de la connectivité Bluetooth
                        c) Des éléments pour la gestion des requêtes HTTP, la validation des données, etc.
                        d) Des outils pour la conception de bases de données relationnelles

                        Comment Symfony peut-il être installé ?
                        a) En utilisant Docker exclusivement
                        b) En utilisant un gestionnaire de dépendances pour PHP appelé Composer
                        c) En téléchargeant manuellement les fichiers nécessaires depuis le site web de Symfony
                        d) En utilisant un outil d'installation spécifique à chaque système d'exploitation

                        Quelle est l'étape nécessaire après l'installation de Symfony ?
                        a) Configuration des paramètres du projet et définition des routes initiales
                        b) Aucune étape supplémentaire n'est nécessaire, Symfony est prêt à être utilisé immédiatement
                        c) Redémarrage du système pour que les changements prennent effet
                        d) Installation de modules complémentaires pour étendre les fonctionnalités de Symfony

                        """, symfonyIntroductionInstallation);

        Quiz symfonyStructure = new Quiz("Evaluation sur l'architecture Symfony",
                """
                        Ce quiz porte sur les concepts fondamentaux de l'architecture MVC (Modèle-Vue-Contrôleur) dans le contexte de Symfony, ainsi que sur la structure des répertoires dans un projet Symfony. Il vise à évaluer la compréhension des apprenants concernant le rôle de chaque composant MVC, l'organisation des fichiers et des répertoires dans un projet Symfony, ainsi que les bonnes pratiques associées à la gestion des fichiers et des ressources.
                        Quel est le rôle du Contrôleur dans l'architecture MVC ?
                        a) Gérer la logique métier et les données
                        b) Gérer l'interface utilisateur et l'affichage des données
                        c) Traiter les requêtes HTTP et coordonner les actions
                        d) Représenter les données de l'application

                        Dans Symfony, où se situe généralement la logique métier de l'application ?
                        a) Dans le Contrôleur
                        b) Dans la Vue
                        c) Dans le Modèle
                        d) Dans les fichiers de configuration

                        Quel est l'un des avantages de la structure MVC dans Symfony ?
                        a) Elle simplifie la gestion des fichiers et des répertoires
                        b) Elle facilite la collaboration entre les membres de l'équipe de développement
                        c) Elle rend l'application plus rapide
                        d) Elle garantit la sécurité de l'application

                        Quel répertoire est principalement utilisé pour stocker les fichiers de configuration dans un projet Symfony ?
                        a) src
                        b) config
                        c) templates
                        d) vendor

                        Quel est le répertoire utilisé pour stocker les vues dans Symfony ?
                        a) src
                        b) config
                        c) templates
                        d) vendor

                        """, symfonyStructureAppStructures);

        Quiz symfonyRoutes = new Quiz("Evaluation sur la configuration des routes et création des controlleurs",
                """
                        Ce quiz porte sur la configuration des routes et la création de contrôleurs dans Symfony, un framework PHP. Il vise à évaluer la compréhension des concepts fondamentaux liés à la gestion des routes et des contrôleurs dans le développement d'applications Symfony. Les questions abordent des sujets tels que la définition des routes dans Symfony, la syntaxe des routes, le rôle des contrôleurs, et l'importance de l'injection de dépendances. Ce quiz est destiné à tester les connaissances des apprenants sur ces concepts clés et à les aider à consolider leur compréhension de la structure et du fonctionnement des applications Symfony.
                        Quel est le rôle principal de la configuration des routes dans Symfony ?
                        a) Diriger les requêtes HTTP vers les actions de contrôleurs appropriées
                        b) Gérer les dépendances entre les composants de l'application
                        c) Générer automatiquement le code pour les contrôleurs
                        d) Contrôler l'accès aux ressources statiques de l'application

                        Où pouvez-vous définir les routes dans Symfony ?
                        a) Dans le fichier controllers.yaml
                        b) Dans le fichier services.yaml
                        c) Dans le fichier routes.yaml
                        d) Dans le fichier config.yaml

                        Quelle est la syntaxe utilisée pour définir des paramètres dynamiques dans une route Symfony ?
                        a) {param}
                        b) [param]
                        c) <param>
                        d) %param%

                        Quel est le rôle principal des contrôleurs dans Symfony ?
                        a) Gérer les dépendances de l'application
                        b) Générer des vues HTML
                        c) Traiter les requêtes HTTP et exécuter la logique métier
                        d) Interagir avec la base de données

                        Pourquoi l'injection de dépendances est-elle importante dans les contrôleurs Symfony ?
                        a) Pour éviter les erreurs de syntaxe
                        b) Pour rendre les contrôleurs plus complexes
                        c) Pour accéder à d'autres services et composants de l'application
                        d) Pour limiter l'accès aux ressources de l'application
                        
                        """, symfonyRoutesControllersController);

        Quiz symfonyVuesT = new Quiz("Evaluation sur l'Introduction à Twig et Intégration des Vues dans Symfony",
                """
                        Ce quiz porte sur les concepts fondamentaux de Twig et son intégration dans Symfony pour la création de vues dynamiques dans les applications web. Les questions couvrent les bases de Twig, y compris son rôle dans Symfony, sa syntaxe de base, ainsi que son utilisation pour générer des vues et partager des données entre les contrôleurs et les vues. Les réponses à choix unique permettent de tester la compréhension des apprenants sur ces sujets et de vérifier s'ils sont capables d'appliquer ces connaissances dans le développement d'applications Symfony.
                        Qu'est-ce que Twig dans le contexte de Symfony ?
                        a) Un langage de programmation pour la manipulation des bases de données
                        b) Un moteur de template utilisé pour générer des vues HTML dans Symfony
                        c) Un framework PHP pour le développement d'applications web
                        d) Un outil de gestion des dépendances pour les projets Symfony

                        Quel est l'avantage principal de l'utilisation de Twig par rapport à d'autres moteurs de template ?
                        a) Sa complexité syntaxique
                        b) Sa faible intégration avec Symfony
                        c) Sa flexibilité et sa puissance
                        d) Son incompatibilité avec les navigateurs web modernes

                        Pour le sous-chapitre "Intégration des Vues dans Symfony" :

                        Comment peut-on utiliser Twig dans Symfony pour générer des vues ?
                        a) En écrivant du code JavaScript pur
                        b) En utilisant des balises HTML classiques
                        c) En créant des fichiers de vue avec une syntaxe Twig spécifique
                        d) En reliant directement les contrôleurs aux vues sans Twig

                        Quelles sont les méthodes pour partager des données entre le contrôleur et la vue dans Symfony ?
                        a) En utilisant uniquement des sessions PHP
                        b) En stockant les données dans des fichiers texte
                        c) En passant des variables et en utilisant des fonctions Twig spécifiques
                        d) En les exposant directement dans l'URL de la page

                        """, symfonyVuesTwigIntegration);

        Quiz symfonyFormulaireCreateTeatement = new Quiz("Evaluation sur la création et le traitement des données de Formulaires",
                """
                        Ce quiz porte sur la création et le traitement des formulaires avec Symfony, un framework PHP populaire pour le développement d'applications web. Les questions couvrent les concepts clés liés à la création de formulaires à l'aide des classes de formulaire Symfony, ainsi que la validation des données soumises et la gestion des erreurs. Les apprenants seront évalués sur leur compréhension de l'utilisation des classes de formulaire Symfony, de la validation des données, de la récupération et de la manipulation des données soumises, ainsi que de la gestion des erreurs de formulaire.
                        Quelle est l'utilité principale des classes de formulaire Symfony ?
                        a) Gérer les requêtes HTTP
                        b) Générer des vues HTML
                        c) Créer et gérer des formulaires
                        d) Valider les données de formulaire

                        Quel est l'objectif de la validation des données de formulaire dans Symfony ?
                        a) Empêcher toute soumission de formulaire
                        b) S'assurer que les données soumises respectent les règles définies
                        c) Générer des messages d'erreur personnalisés
                        d) Créer des formulaires dynamiques

                        Comment récupère-t-on les données soumises à partir du formulaire dans Symfony ?
                        a) En utilisant des classes de contrôleur dédiées
                        b) En accédant directement à la base de données
                        c) En utilisant les fonctions JavaScript côté client
                        d) En utilisant les outils fournis par Symfony pour traiter les données

                        Quelle est l'importance de la gestion des erreurs de formulaire ?
                        a) Améliorer la sécurité de l'application
                        b) Empêcher les utilisateurs d'accéder aux formulaires
                        c) Aider les utilisateurs à corriger les données incorrectes
                        d) Simplifier le processus de développement des formulaires

                        """, symfonyFormulaireTraitement);

        trainingRepository.save(symfony);

        quizRepository.saveAll(List.of(IntroductionDefInstall, symfonyStructure, symfonyRoutes, symfonyStructure, symfonyVuesT, symfonyFormulaireCreateTeatement));
    }
}
