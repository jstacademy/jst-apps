package com.jst.backend.Training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.jst.backend.Training.TrainingMapper.toTrainingListSimpleDTOS;

@RestController
@RequestMapping("/trainings")
@CrossOrigin(origins = "*")
public class TrainingController {

    @Autowired
    private TrainingService trainingService;

    // Get all trainings
    @GetMapping
    public ResponseEntity<List<TrainingSimpleDTO>> getAllTrainings() {
        return ResponseEntity.ok(toTrainingListSimpleDTOS(trainingService.getAllTrainings()));
    }

    // Get a single training by ID
    @GetMapping("/{id}")
    public ResponseEntity<Training> getTrainingById(@PathVariable Long id) {
        Training training = trainingService.getTrainingById(id);
        return ResponseEntity.ok(training);
    }

    // Create a new training
    @PostMapping
    public ResponseEntity<Training> createTraining(@RequestBody TrainingSimpleDTO training) {
        Training savedTraining = trainingService.createTraining(
                new Training(training.getTitle(),
                        training.getDescription()
                )
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(savedTraining);
    }

    // Update an existing training
    @PutMapping("/{id}")
    public ResponseEntity<Training> updateTraining(@PathVariable Long id, @RequestBody TrainingSimpleDTO trainingDetails) {
        Training updatedTraining = trainingService.updateTraining(id,
                new Training(trainingDetails.getTitle(),
                        trainingDetails.getDescription()
                )
        );
        return ResponseEntity.ok(updatedTraining);
    }

    // Delete a training
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTraining(@PathVariable Long id) {
        trainingService.deleteTraining(id);
        return ResponseEntity.noContent().build();
    }
}
