package com.jst.backend.Training;

import java.util.ArrayList;
import java.util.List;

public class TrainingMapper {
    public static TrainingSimpleDTO toTrainingSimpleDTO(Training training) {
        TrainingSimpleDTO trainingSimpleDTO = new TrainingSimpleDTO();
        trainingSimpleDTO.setTrainingId(training.getTrainingId());
        trainingSimpleDTO.setTitle(training.getTitle());
        trainingSimpleDTO.setDescription(training.getDescription());
        return trainingSimpleDTO;
    }

    public static List<TrainingSimpleDTO> toTrainingListSimpleDTOS(List<Training> trainings) {
        List<TrainingSimpleDTO> trainingSimpleDTOS = new ArrayList<>();
        for (Training training : trainings) {
            TrainingSimpleDTO trainingSimpleDTO = toTrainingSimpleDTO(training);
            trainingSimpleDTOS.add(trainingSimpleDTO);
        }
        return trainingSimpleDTOS;
    }
}
