package com.jst.backend.Training;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.jst.backend.Course.Course;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Entity
@Table(name = "training")
public class Training {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long trainingId;

    @Setter
    @Column(nullable = false, length = 255)
    private String title;

    @Setter
    @Column(nullable = false, length = 1000)
    private String description;

    @OneToMany(mappedBy = "training", cascade = CascadeType.ALL)
    @Setter
    @JsonManagedReference
    private Set<Course> courses;

    public Training(String title, String description) {
        this.title = title;
        this.description = description;
        this.courses = new HashSet<>();
    }

    public Training() {
        this.courses = new HashSet<>();
    }

    public void addCourse(Course course) {
        courses.add(course);
        course.setTraining(this);
    }

    public void removeCourse(Course course) {
        courses.remove(course);
        course.setTraining(null);
    }
}
