package com.jst.backend.Training;

import jakarta.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TrainingSimpleDTO {
    @Nullable
    private Long trainingId;
    private String title;
    private String description;
}
