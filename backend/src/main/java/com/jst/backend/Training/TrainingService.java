package com.jst.backend.Training;

import com.jst.backend.Exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrainingService {

    @Autowired
    private TrainingRepository trainingRepository;

    // Retrieve all trainings
    public List<Training> getAllTrainings() {
        return trainingRepository.findAll();
    }

    // Retrieve a single training by ID
    public Training getTrainingById(Long id) {
        Optional<Training> training = trainingRepository.findById(id);
        if (training.isPresent()) {
            return training.get();
        } else {
            // Handle the case where the training is not found
            throw new ResourceNotFoundException("Training not found for this id: " + id);
        }
    }

    // Create a new training
    public Training createTraining(Training training) {
        return trainingRepository.save(training);
    }

    // Update an existing training
    public Training updateTraining(Long id, Training trainingDetails) {
        Training training = getTrainingById(id);

        // Update fields of training
        training.setTitle(trainingDetails.getTitle());
        training.setDescription(trainingDetails.getDescription());

        return trainingRepository.save(training);
    }

    // Delete a training
    public void deleteTraining(Long id) {
        Training training = getTrainingById(id);
        trainingRepository.delete(training);
    }
}
