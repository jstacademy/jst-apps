package com.jst.backend.Question;

import com.jst.backend.Quiz.Quiz;
import com.jst.backend.Quiz.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/question")
@CrossOrigin(origins = "*")
public class QuestionController {
    private final QuestionService questionService;
    private final QuizService quizService;

    @Autowired
    public QuestionController(QuestionService questionService, QuizService quizService) {
        this.questionService = questionService;
        this.quizService = quizService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Question> getQuestionById(@PathVariable Long id) {
        return questionService.getQuestionById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/byQuiz/{quizId}")
    public ResponseEntity<List<Question>> getQuestionByQuizId(@PathVariable Long quizId) {
        List<Question> questions = questionService.getQuestionByQuizId(quizId);
        if (questions.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(questions);
    }

    @PostMapping()
    public ResponseEntity<Object> createQuestion(@RequestBody QuestionDto questionDto) {
        Quiz quiz = quizService.getQuizById(questionDto.getQuiz()).orElse(null);
        if (quiz == null) {
            return ResponseEntity.notFound().build();
        }
        Question question = new Question(questionDto.getQuestion(), quiz);
        question = questionService.saveQuestion(question);
        return ResponseEntity.ok(question);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Question> updateQuestion(@PathVariable Long id, @RequestBody Question question) {
        return ResponseEntity.ok(questionService.updateQuestion(id, question));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable Long id) {
        if (questionService.getQuestionById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        questionService.deleteQuestion(id);
        return ResponseEntity.ok().build();
    }
}
