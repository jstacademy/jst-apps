package com.jst.backend.Question;

import com.jst.backend.Answer.Answer;
import com.jst.backend.Answer.AnswerDto;
import jakarta.annotation.Nullable;
import lombok.Getter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
public class QuestionDto {
    private String question;

    @Nullable
    private Long quiz;
    private List<AnswerDto> answers;

    public Set<Answer> getAnswers(Question question) {
        Set<Answer> answerList = new HashSet<>();
        for (AnswerDto answerDto : answers) {
            answerList.add(new Answer(
                    answerDto.getAnswer(),
                    answerDto.isTrue(),
                    question
            ));
        }
        return answerList;
    }

    @Override
    public String toString() {
        return "QuestionDto{" +
                "question='" + question + '\'' +
                ", quiz=" + quiz +
                ", answers=" + answers.toString() +
                '}';
    }
}
