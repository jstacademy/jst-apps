package com.jst.backend.Question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class QuestionService {
    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public Optional<Question> getQuestionById(Long id) {
        return questionRepository.findById(id);
    }

    public List<Question> getQuestionByQuizId(Long quizId) {
        return questionRepository.findQuestionByQuizQuizId(quizId);
    }

    @Transactional
    public Question saveQuestion(Question question) {
        return questionRepository.save(question);
    }

    @Transactional
    public Question updateQuestion(Long id, Question questionParam) {
        Question question = questionRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException(
                        "Question with the id " + id + " does not exist !"
                ));
        if (questionParam.getQuestion() != null &&
                questionParam.getQuestion().length() > 2 &&
                !Objects.equals(question.getQuestion(), questionParam.getQuestion())) {
            question.setQuestion(questionParam.getQuestion());
        }
        if (questionParam.getQuiz() != null &&
                !Objects.equals(question.getQuiz(), questionParam.getQuiz())) {
            question.setQuiz(questionParam.getQuiz());
        }
        return questionRepository.save(question);
    }

    @Transactional
    public void deleteQuestion(Long id) {
        questionRepository.deleteById(id);
    }
}
