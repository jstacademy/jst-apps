package com.jst.backend.Question;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.jst.backend.Answer.Answer;
import com.jst.backend.Quiz.Quiz;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long questionId;

    @Column(nullable = false, length = 500)
    private String question;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "quiz_id", nullable = false)
    @JsonIgnore
    private Quiz quiz;

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Answer> answers;

    public Question(String question, Quiz quiz) {
        this.question = question;
        this.quiz = quiz;
        this.quiz.addQuestion(this);
        this.answers = new HashSet<>();
    }

    public Question() {
    }

    public void addAnswer(Answer answer) {
        this.answers.add(answer);
    }

    public void addAnswers(Set<Answer> answers) {
        for (Answer answer : answers) {
            addAnswer(answer);
        }
    }

    // Getters, setters, and constructors
}
